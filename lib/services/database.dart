import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/models/serializers.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

const String defaultDbName = 'weather.db';

const String table = 'location';
const String columnId = 'id';
const String columnName = 'name';
const String columnCountry = 'country';

class DatabaseProvider {
  Database _db;

  Future<void> open([String dbName]) async {
    final String basePath = await getDatabasesPath();
    final String path = join(basePath, dbName ?? defaultDbName);

    _db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
              create table $table (
                  $columnId integer primary key,
                  $columnName text not null,
                  $columnCountry text not null
              )
              ''');
    });
  }

  Future<void> deleteDb([String dbName]) async {
    final String basePath = await getDatabasesPath();
    final String path = join(basePath, dbName ?? defaultDbName);
    await deleteDatabase(path);
  }

  Future<void> insertLocation(SavedLocation location) async {
    if (_db == null) {
      await open();
    }

    final Map<String, dynamic> map =
        serializers.serializeWith(SavedLocation.serializer, location);
    await _db.insert(
      table,
      map,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<SavedLocation>> getSavedLocations() async {
    if (_db == null) {
      await open();
    }

    final List<Map<String, dynamic>> maps = await _db.query(table);

    if (maps.isNotEmpty) {
      return maps
          .map((Map<String, dynamic> map) =>
              serializers.deserializeWith(SavedLocation.serializer, map))
          .toList();
    }

    return null;
  }

  Future<void> close() async {
    await _db.close();
  }
}
