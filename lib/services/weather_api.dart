import 'dart:convert';
import 'dart:io';

import 'package:flutter_redux_weather/models/search_result.dart';
import 'package:flutter_redux_weather/models/serializers.dart';
import 'package:flutter_redux_weather/models/weather_forecast.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';

final Uri baseUri = Uri.parse('https://api.openweathermap.org/data/2.5/');

class WeatherApi {
  WeatherApi(this.apiKey) : _client = _WeatherApiClient(apiKey, 'metric');

  final String apiKey;
  final BaseClient _client;

  Future<SearchResult> findCity(String query) async {
    final Uri uri = Uri.https(
      baseUri.authority,
      '${baseUri.path}/find',
      <String, String>{
        'q': query,
      },
    );

    final Response response = await _client.get(uri);

    _checkError(response);

    final Map<String, dynamic> responseJson = json.decode(response.body);
    final SearchResult result =
        serializers.deserializeWith(SearchResult.serializer, responseJson);

    return result;
  }

  Future<WeatherLocation> queryLocation({int id, Coord coord}) async {
    final Map<String, String> params = _buildParams(id: id, coord: coord);

    final Uri uri = Uri.https(
      baseUri.authority,
      '${baseUri.path}/weather',
      params,
    );

    final Response response = await _client.get(uri);

    _checkError(response);

    final Map<String, dynamic> responseJson = json.decode(response.body);
    final WeatherLocation result =
        serializers.deserializeWith(WeatherLocation.serializer, responseJson);

    return result;
  }

  Future<WeatherForecast> queryForecast({int id, Coord coord}) async {
    final Map<String, String> params = _buildParams(id: id, coord: coord);
    final Uri uri = Uri.https(
      baseUri.authority,
      '${baseUri.path}/forecast',
      params,
    );

    final Response response = await _client.get(uri);

    _checkError(response);

    final Map<String, dynamic> responseJson = json.decode(response.body);
    final WeatherForecast result =
        serializers.deserializeWith(WeatherForecast.serializer, responseJson);

    return result;
  }

  Map<String, String> _buildParams({int id, Coord coord}) {
    final Map<String, String> params = <String, String>{};
    if (id != null) {
      params['id'] = id.toString();
    }

    if (coord != null) {
      params['lat'] = coord.lat.toString();
      params['lon'] = coord.lon.toString();
    }

    return params;
  }

  void _checkError(Response response) {
    if (response.statusCode != HttpStatus.ok) {
      try {
        final Map<String, dynamic> errors = json.decode(response.body);
        throw Exception('Error: ${errors['message']}');
      } catch (e) {
        throw Exception('Unknown api error');
      }
    }
  }
}

class _WeatherApiClient extends BaseClient {
  _WeatherApiClient(this.apiKey, this.units) : _innerClient = IOClient();

  final String apiKey;
  final String units;
  final IOClient _innerClient;

  @override
  Future<StreamedResponse> send(BaseRequest request) async {
    final Request newRequest = Request(
      request.method,
      Uri(
        scheme: request.url.scheme,
        userInfo: request.url.userInfo,
        host: request.url.host,
        port: request.url.port,
        pathSegments: request.url.pathSegments,
        queryParameters: Map<String, String>.from(request.url.queryParameters)
          ..addAll(<String, String>{
            'APPID': apiKey,
            'units': units,
          }),
        fragment: request.url.fragment,
      ),
    );
    return _innerClient.send(newRequest);
  }
}
