// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(City.serializer)
      ..add(Clouds.serializer)
      ..add(Coord.serializer)
      ..add(ForecastItem.serializer)
      ..add(Main.serializer)
      ..add(Rain.serializer)
      ..add(SavedLocation.serializer)
      ..add(SearchResult.serializer)
      ..add(Snow.serializer)
      ..add(Sys.serializer)
      ..add(Weather.serializer)
      ..add(WeatherForecast.serializer)
      ..add(WeatherLocation.serializer)
      ..add(Wind.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ForecastItem)]),
          () => new ListBuilder<ForecastItem>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Weather)]),
          () => new ListBuilder<Weather>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Weather)]),
          () => new ListBuilder<Weather>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(WeatherLocation)]),
          () => new ListBuilder<WeatherLocation>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
