import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'saved_location.g.dart';

abstract class SavedLocation
    implements Built<SavedLocation, SavedLocationBuilder> {
  factory SavedLocation([updates(SavedLocationBuilder b)]) = _$SavedLocation;
  SavedLocation._();

  int get id;
  String get name;
  String get country;

  static Serializer<SavedLocation> get serializer => _$savedLocationSerializer;
}
