import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_redux_weather/models/saved_location.dart';

part 'weather_location.g.dart';

abstract class WeatherLocation
    implements Built<WeatherLocation, WeatherLocationBuilder> {
  factory WeatherLocation([updates(WeatherLocationBuilder b)]) =
      _$WeatherLocation;
  WeatherLocation._();

  int get id;
  String get name;

  /// unix timestamp of data calculation in UTC
  int get dt;
  Coord get coord;
  BuiltList<Weather> get weather;
  Main get main;
  Wind get wind;
  Clouds get clouds;
  @nullable
  Rain get rain;
  @nullable
  Snow get snow;
  Sys get sys;

  SavedLocation asSavedLocation() {
    return SavedLocation(
      (SavedLocationBuilder b) => b
        ..id = id
        ..name = name
        ..country = sys.country,
    );
  }

  static Serializer<WeatherLocation> get serializer =>
      _$weatherLocationSerializer;
}

abstract class Coord implements Built<Coord, CoordBuilder> {
  factory Coord([updates(CoordBuilder b)]) = _$Coord;
  Coord._();

  double get lon;
  double get lat;

  static Serializer<Coord> get serializer => _$coordSerializer;
}

abstract class Main implements Built<Main, MainBuilder> {
  factory Main([updates(MainBuilder b)]) = _$Main;
  Main._();

  double get temp;
  double get pressure;
  int get humidity;
  @BuiltValueField(wireName: 'temp_min')
  double get tempMin;
  @BuiltValueField(wireName: 'temp_max')
  double get tempMax;
  @nullable
  @BuiltValueField(wireName: 'sea_level')
  double get seaLevel;
  @nullable
  @BuiltValueField(wireName: 'grnd_level')
  double get grndLevel;

  static Serializer<Main> get serializer => _$mainSerializer;
}

abstract class Wind implements Built<Wind, WindBuilder> {
  factory Wind([updates(WindBuilder b)]) = _$Wind;
  Wind._();

  @nullable
  double get speed;
  @nullable
  double get deg;

  static Serializer<Wind> get serializer => _$windSerializer;
}

abstract class Weather implements Built<Weather, WeatherBuilder> {
  factory Weather([updates(WeatherBuilder b)]) = _$Weather;
  Weather._();

  int get id;
  String get main;
  String get description;
  String get icon;

  static Serializer<Weather> get serializer => _$weatherSerializer;
}

abstract class Clouds implements Built<Clouds, CloudsBuilder> {
  factory Clouds([updates(CloudsBuilder b)]) = _$Clouds;
  Clouds._();

  int get all;

  static Serializer<Clouds> get serializer => _$cloudsSerializer;
}

abstract class Rain implements Built<Rain, RainBuilder> {
  factory Rain([updates(RainBuilder b)]) = _$Rain;
  Rain._();

  @nullable
  @BuiltValueField(wireName: '1h')
  double get oneHour;
  @nullable
  @BuiltValueField(wireName: '3h')
  double get threeHours;

  static Serializer<Rain> get serializer => _$rainSerializer;
}

abstract class Snow implements Built<Snow, SnowBuilder> {
  factory Snow([updates(SnowBuilder b)]) = _$Snow;
  Snow._();

  @nullable
  @BuiltValueField(wireName: '1h')
  double get oneHour;
  @nullable
  @BuiltValueField(wireName: '3h')
  double get threeHours;

  static Serializer<Snow> get serializer => _$snowSerializer;
}

abstract class Sys implements Built<Sys, SysBuilder> {
  factory Sys([updates(SysBuilder b)]) = _$Sys;
  Sys._();

  @nullable
  String get country;
  @nullable
  int get sunrise;
  @nullable
  int get sunset;

  static Serializer<Sys> get serializer => _$sysSerializer;
}
