import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';

part 'search_result.g.dart';

abstract class SearchResult
    implements Built<SearchResult, SearchResultBuilder> {
  factory SearchResult([updates(SearchResultBuilder b)]) = _$SearchResult;
  SearchResult._();

  String get message;
  String get cod;
  int get count;
  BuiltList<WeatherLocation> get list;

  static Serializer<SearchResult> get serializer => _$searchResultSerializer;
}
