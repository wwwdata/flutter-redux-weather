// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_location.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherLocation> _$weatherLocationSerializer =
    new _$WeatherLocationSerializer();
Serializer<Coord> _$coordSerializer = new _$CoordSerializer();
Serializer<Main> _$mainSerializer = new _$MainSerializer();
Serializer<Wind> _$windSerializer = new _$WindSerializer();
Serializer<Weather> _$weatherSerializer = new _$WeatherSerializer();
Serializer<Clouds> _$cloudsSerializer = new _$CloudsSerializer();
Serializer<Rain> _$rainSerializer = new _$RainSerializer();
Serializer<Snow> _$snowSerializer = new _$SnowSerializer();
Serializer<Sys> _$sysSerializer = new _$SysSerializer();

class _$WeatherLocationSerializer
    implements StructuredSerializer<WeatherLocation> {
  @override
  final Iterable<Type> types = const [WeatherLocation, _$WeatherLocation];
  @override
  final String wireName = 'WeatherLocation';

  @override
  Iterable serialize(Serializers serializers, WeatherLocation object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'dt',
      serializers.serialize(object.dt, specifiedType: const FullType(int)),
      'coord',
      serializers.serialize(object.coord, specifiedType: const FullType(Coord)),
      'weather',
      serializers.serialize(object.weather,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Weather)])),
      'main',
      serializers.serialize(object.main, specifiedType: const FullType(Main)),
      'wind',
      serializers.serialize(object.wind, specifiedType: const FullType(Wind)),
      'clouds',
      serializers.serialize(object.clouds,
          specifiedType: const FullType(Clouds)),
      'sys',
      serializers.serialize(object.sys, specifiedType: const FullType(Sys)),
    ];
    if (object.rain != null) {
      result
        ..add('rain')
        ..add(serializers.serialize(object.rain,
            specifiedType: const FullType(Rain)));
    }
    if (object.snow != null) {
      result
        ..add('snow')
        ..add(serializers.serialize(object.snow,
            specifiedType: const FullType(Snow)));
    }

    return result;
  }

  @override
  WeatherLocation deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherLocationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dt':
          result.dt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'coord':
          result.coord.replace(serializers.deserialize(value,
              specifiedType: const FullType(Coord)) as Coord);
          break;
        case 'weather':
          result.weather.replace(serializers.deserialize(value,
              specifiedType: const FullType(
                  BuiltList, const [const FullType(Weather)])) as BuiltList);
          break;
        case 'main':
          result.main.replace(serializers.deserialize(value,
              specifiedType: const FullType(Main)) as Main);
          break;
        case 'wind':
          result.wind.replace(serializers.deserialize(value,
              specifiedType: const FullType(Wind)) as Wind);
          break;
        case 'clouds':
          result.clouds.replace(serializers.deserialize(value,
              specifiedType: const FullType(Clouds)) as Clouds);
          break;
        case 'rain':
          result.rain.replace(serializers.deserialize(value,
              specifiedType: const FullType(Rain)) as Rain);
          break;
        case 'snow':
          result.snow.replace(serializers.deserialize(value,
              specifiedType: const FullType(Snow)) as Snow);
          break;
        case 'sys':
          result.sys.replace(serializers.deserialize(value,
              specifiedType: const FullType(Sys)) as Sys);
          break;
      }
    }

    return result.build();
  }
}

class _$CoordSerializer implements StructuredSerializer<Coord> {
  @override
  final Iterable<Type> types = const [Coord, _$Coord];
  @override
  final String wireName = 'Coord';

  @override
  Iterable serialize(Serializers serializers, Coord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'lon',
      serializers.serialize(object.lon, specifiedType: const FullType(double)),
      'lat',
      serializers.serialize(object.lat, specifiedType: const FullType(double)),
    ];

    return result;
  }

  @override
  Coord deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CoordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'lon':
          result.lon = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$MainSerializer implements StructuredSerializer<Main> {
  @override
  final Iterable<Type> types = const [Main, _$Main];
  @override
  final String wireName = 'Main';

  @override
  Iterable serialize(Serializers serializers, Main object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'temp',
      serializers.serialize(object.temp, specifiedType: const FullType(double)),
      'pressure',
      serializers.serialize(object.pressure,
          specifiedType: const FullType(double)),
      'humidity',
      serializers.serialize(object.humidity,
          specifiedType: const FullType(int)),
      'temp_min',
      serializers.serialize(object.tempMin,
          specifiedType: const FullType(double)),
      'temp_max',
      serializers.serialize(object.tempMax,
          specifiedType: const FullType(double)),
    ];
    if (object.seaLevel != null) {
      result
        ..add('sea_level')
        ..add(serializers.serialize(object.seaLevel,
            specifiedType: const FullType(double)));
    }
    if (object.grndLevel != null) {
      result
        ..add('grnd_level')
        ..add(serializers.serialize(object.grndLevel,
            specifiedType: const FullType(double)));
    }

    return result;
  }

  @override
  Main deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MainBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'temp':
          result.temp = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'pressure':
          result.pressure = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'humidity':
          result.humidity = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'temp_min':
          result.tempMin = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'temp_max':
          result.tempMax = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'sea_level':
          result.seaLevel = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'grnd_level':
          result.grndLevel = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$WindSerializer implements StructuredSerializer<Wind> {
  @override
  final Iterable<Type> types = const [Wind, _$Wind];
  @override
  final String wireName = 'Wind';

  @override
  Iterable serialize(Serializers serializers, Wind object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.speed != null) {
      result
        ..add('speed')
        ..add(serializers.serialize(object.speed,
            specifiedType: const FullType(double)));
    }
    if (object.deg != null) {
      result
        ..add('deg')
        ..add(serializers.serialize(object.deg,
            specifiedType: const FullType(double)));
    }

    return result;
  }

  @override
  Wind deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WindBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'speed':
          result.speed = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'deg':
          result.deg = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherSerializer implements StructuredSerializer<Weather> {
  @override
  final Iterable<Type> types = const [Weather, _$Weather];
  @override
  final String wireName = 'Weather';

  @override
  Iterable serialize(Serializers serializers, Weather object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'main',
      serializers.serialize(object.main, specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'icon',
      serializers.serialize(object.icon, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Weather deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'main':
          result.main = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'icon':
          result.icon = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CloudsSerializer implements StructuredSerializer<Clouds> {
  @override
  final Iterable<Type> types = const [Clouds, _$Clouds];
  @override
  final String wireName = 'Clouds';

  @override
  Iterable serialize(Serializers serializers, Clouds object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'all',
      serializers.serialize(object.all, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  Clouds deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CloudsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'all':
          result.all = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$RainSerializer implements StructuredSerializer<Rain> {
  @override
  final Iterable<Type> types = const [Rain, _$Rain];
  @override
  final String wireName = 'Rain';

  @override
  Iterable serialize(Serializers serializers, Rain object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.oneHour != null) {
      result
        ..add('1h')
        ..add(serializers.serialize(object.oneHour,
            specifiedType: const FullType(double)));
    }
    if (object.threeHours != null) {
      result
        ..add('3h')
        ..add(serializers.serialize(object.threeHours,
            specifiedType: const FullType(double)));
    }

    return result;
  }

  @override
  Rain deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RainBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '1h':
          result.oneHour = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case '3h':
          result.threeHours = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$SnowSerializer implements StructuredSerializer<Snow> {
  @override
  final Iterable<Type> types = const [Snow, _$Snow];
  @override
  final String wireName = 'Snow';

  @override
  Iterable serialize(Serializers serializers, Snow object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.oneHour != null) {
      result
        ..add('1h')
        ..add(serializers.serialize(object.oneHour,
            specifiedType: const FullType(double)));
    }
    if (object.threeHours != null) {
      result
        ..add('3h')
        ..add(serializers.serialize(object.threeHours,
            specifiedType: const FullType(double)));
    }

    return result;
  }

  @override
  Snow deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SnowBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '1h':
          result.oneHour = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case '3h':
          result.threeHours = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$SysSerializer implements StructuredSerializer<Sys> {
  @override
  final Iterable<Type> types = const [Sys, _$Sys];
  @override
  final String wireName = 'Sys';

  @override
  Iterable serialize(Serializers serializers, Sys object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.country != null) {
      result
        ..add('country')
        ..add(serializers.serialize(object.country,
            specifiedType: const FullType(String)));
    }
    if (object.sunrise != null) {
      result
        ..add('sunrise')
        ..add(serializers.serialize(object.sunrise,
            specifiedType: const FullType(int)));
    }
    if (object.sunset != null) {
      result
        ..add('sunset')
        ..add(serializers.serialize(object.sunset,
            specifiedType: const FullType(int)));
    }

    return result;
  }

  @override
  Sys deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SysBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sunrise':
          result.sunrise = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'sunset':
          result.sunset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherLocation extends WeatherLocation {
  @override
  final int id;
  @override
  final String name;
  @override
  final int dt;
  @override
  final Coord coord;
  @override
  final BuiltList<Weather> weather;
  @override
  final Main main;
  @override
  final Wind wind;
  @override
  final Clouds clouds;
  @override
  final Rain rain;
  @override
  final Snow snow;
  @override
  final Sys sys;

  factory _$WeatherLocation([void updates(WeatherLocationBuilder b)]) =>
      (new WeatherLocationBuilder()..update(updates)).build();

  _$WeatherLocation._(
      {this.id,
      this.name,
      this.dt,
      this.coord,
      this.weather,
      this.main,
      this.wind,
      this.clouds,
      this.rain,
      this.snow,
      this.sys})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'name');
    }
    if (dt == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'dt');
    }
    if (coord == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'coord');
    }
    if (weather == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'weather');
    }
    if (main == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'main');
    }
    if (wind == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'wind');
    }
    if (clouds == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'clouds');
    }
    if (sys == null) {
      throw new BuiltValueNullFieldError('WeatherLocation', 'sys');
    }
  }

  @override
  WeatherLocation rebuild(void updates(WeatherLocationBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherLocationBuilder toBuilder() =>
      new WeatherLocationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherLocation &&
        id == other.id &&
        name == other.name &&
        dt == other.dt &&
        coord == other.coord &&
        weather == other.weather &&
        main == other.main &&
        wind == other.wind &&
        clouds == other.clouds &&
        rain == other.rain &&
        snow == other.snow &&
        sys == other.sys;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                                        dt.hashCode),
                                    coord.hashCode),
                                weather.hashCode),
                            main.hashCode),
                        wind.hashCode),
                    clouds.hashCode),
                rain.hashCode),
            snow.hashCode),
        sys.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherLocation')
          ..add('id', id)
          ..add('name', name)
          ..add('dt', dt)
          ..add('coord', coord)
          ..add('weather', weather)
          ..add('main', main)
          ..add('wind', wind)
          ..add('clouds', clouds)
          ..add('rain', rain)
          ..add('snow', snow)
          ..add('sys', sys))
        .toString();
  }
}

class WeatherLocationBuilder
    implements Builder<WeatherLocation, WeatherLocationBuilder> {
  _$WeatherLocation _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _dt;
  int get dt => _$this._dt;
  set dt(int dt) => _$this._dt = dt;

  CoordBuilder _coord;
  CoordBuilder get coord => _$this._coord ??= new CoordBuilder();
  set coord(CoordBuilder coord) => _$this._coord = coord;

  ListBuilder<Weather> _weather;
  ListBuilder<Weather> get weather =>
      _$this._weather ??= new ListBuilder<Weather>();
  set weather(ListBuilder<Weather> weather) => _$this._weather = weather;

  MainBuilder _main;
  MainBuilder get main => _$this._main ??= new MainBuilder();
  set main(MainBuilder main) => _$this._main = main;

  WindBuilder _wind;
  WindBuilder get wind => _$this._wind ??= new WindBuilder();
  set wind(WindBuilder wind) => _$this._wind = wind;

  CloudsBuilder _clouds;
  CloudsBuilder get clouds => _$this._clouds ??= new CloudsBuilder();
  set clouds(CloudsBuilder clouds) => _$this._clouds = clouds;

  RainBuilder _rain;
  RainBuilder get rain => _$this._rain ??= new RainBuilder();
  set rain(RainBuilder rain) => _$this._rain = rain;

  SnowBuilder _snow;
  SnowBuilder get snow => _$this._snow ??= new SnowBuilder();
  set snow(SnowBuilder snow) => _$this._snow = snow;

  SysBuilder _sys;
  SysBuilder get sys => _$this._sys ??= new SysBuilder();
  set sys(SysBuilder sys) => _$this._sys = sys;

  WeatherLocationBuilder();

  WeatherLocationBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _dt = _$v.dt;
      _coord = _$v.coord?.toBuilder();
      _weather = _$v.weather?.toBuilder();
      _main = _$v.main?.toBuilder();
      _wind = _$v.wind?.toBuilder();
      _clouds = _$v.clouds?.toBuilder();
      _rain = _$v.rain?.toBuilder();
      _snow = _$v.snow?.toBuilder();
      _sys = _$v.sys?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherLocation other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherLocation;
  }

  @override
  void update(void updates(WeatherLocationBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherLocation build() {
    _$WeatherLocation _$result;
    try {
      _$result = _$v ??
          new _$WeatherLocation._(
              id: id,
              name: name,
              dt: dt,
              coord: coord.build(),
              weather: weather.build(),
              main: main.build(),
              wind: wind.build(),
              clouds: clouds.build(),
              rain: _rain?.build(),
              snow: _snow?.build(),
              sys: sys.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'coord';
        coord.build();
        _$failedField = 'weather';
        weather.build();
        _$failedField = 'main';
        main.build();
        _$failedField = 'wind';
        wind.build();
        _$failedField = 'clouds';
        clouds.build();
        _$failedField = 'rain';
        _rain?.build();
        _$failedField = 'snow';
        _snow?.build();
        _$failedField = 'sys';
        sys.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherLocation', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Coord extends Coord {
  @override
  final double lon;
  @override
  final double lat;

  factory _$Coord([void updates(CoordBuilder b)]) =>
      (new CoordBuilder()..update(updates)).build();

  _$Coord._({this.lon, this.lat}) : super._() {
    if (lon == null) {
      throw new BuiltValueNullFieldError('Coord', 'lon');
    }
    if (lat == null) {
      throw new BuiltValueNullFieldError('Coord', 'lat');
    }
  }

  @override
  Coord rebuild(void updates(CoordBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  CoordBuilder toBuilder() => new CoordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Coord && lon == other.lon && lat == other.lat;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, lon.hashCode), lat.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Coord')
          ..add('lon', lon)
          ..add('lat', lat))
        .toString();
  }
}

class CoordBuilder implements Builder<Coord, CoordBuilder> {
  _$Coord _$v;

  double _lon;
  double get lon => _$this._lon;
  set lon(double lon) => _$this._lon = lon;

  double _lat;
  double get lat => _$this._lat;
  set lat(double lat) => _$this._lat = lat;

  CoordBuilder();

  CoordBuilder get _$this {
    if (_$v != null) {
      _lon = _$v.lon;
      _lat = _$v.lat;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Coord other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Coord;
  }

  @override
  void update(void updates(CoordBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Coord build() {
    final _$result = _$v ?? new _$Coord._(lon: lon, lat: lat);
    replace(_$result);
    return _$result;
  }
}

class _$Main extends Main {
  @override
  final double temp;
  @override
  final double pressure;
  @override
  final int humidity;
  @override
  final double tempMin;
  @override
  final double tempMax;
  @override
  final double seaLevel;
  @override
  final double grndLevel;

  factory _$Main([void updates(MainBuilder b)]) =>
      (new MainBuilder()..update(updates)).build();

  _$Main._(
      {this.temp,
      this.pressure,
      this.humidity,
      this.tempMin,
      this.tempMax,
      this.seaLevel,
      this.grndLevel})
      : super._() {
    if (temp == null) {
      throw new BuiltValueNullFieldError('Main', 'temp');
    }
    if (pressure == null) {
      throw new BuiltValueNullFieldError('Main', 'pressure');
    }
    if (humidity == null) {
      throw new BuiltValueNullFieldError('Main', 'humidity');
    }
    if (tempMin == null) {
      throw new BuiltValueNullFieldError('Main', 'tempMin');
    }
    if (tempMax == null) {
      throw new BuiltValueNullFieldError('Main', 'tempMax');
    }
  }

  @override
  Main rebuild(void updates(MainBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MainBuilder toBuilder() => new MainBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Main &&
        temp == other.temp &&
        pressure == other.pressure &&
        humidity == other.humidity &&
        tempMin == other.tempMin &&
        tempMax == other.tempMax &&
        seaLevel == other.seaLevel &&
        grndLevel == other.grndLevel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, temp.hashCode), pressure.hashCode),
                        humidity.hashCode),
                    tempMin.hashCode),
                tempMax.hashCode),
            seaLevel.hashCode),
        grndLevel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Main')
          ..add('temp', temp)
          ..add('pressure', pressure)
          ..add('humidity', humidity)
          ..add('tempMin', tempMin)
          ..add('tempMax', tempMax)
          ..add('seaLevel', seaLevel)
          ..add('grndLevel', grndLevel))
        .toString();
  }
}

class MainBuilder implements Builder<Main, MainBuilder> {
  _$Main _$v;

  double _temp;
  double get temp => _$this._temp;
  set temp(double temp) => _$this._temp = temp;

  double _pressure;
  double get pressure => _$this._pressure;
  set pressure(double pressure) => _$this._pressure = pressure;

  int _humidity;
  int get humidity => _$this._humidity;
  set humidity(int humidity) => _$this._humidity = humidity;

  double _tempMin;
  double get tempMin => _$this._tempMin;
  set tempMin(double tempMin) => _$this._tempMin = tempMin;

  double _tempMax;
  double get tempMax => _$this._tempMax;
  set tempMax(double tempMax) => _$this._tempMax = tempMax;

  double _seaLevel;
  double get seaLevel => _$this._seaLevel;
  set seaLevel(double seaLevel) => _$this._seaLevel = seaLevel;

  double _grndLevel;
  double get grndLevel => _$this._grndLevel;
  set grndLevel(double grndLevel) => _$this._grndLevel = grndLevel;

  MainBuilder();

  MainBuilder get _$this {
    if (_$v != null) {
      _temp = _$v.temp;
      _pressure = _$v.pressure;
      _humidity = _$v.humidity;
      _tempMin = _$v.tempMin;
      _tempMax = _$v.tempMax;
      _seaLevel = _$v.seaLevel;
      _grndLevel = _$v.grndLevel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Main other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Main;
  }

  @override
  void update(void updates(MainBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Main build() {
    final _$result = _$v ??
        new _$Main._(
            temp: temp,
            pressure: pressure,
            humidity: humidity,
            tempMin: tempMin,
            tempMax: tempMax,
            seaLevel: seaLevel,
            grndLevel: grndLevel);
    replace(_$result);
    return _$result;
  }
}

class _$Wind extends Wind {
  @override
  final double speed;
  @override
  final double deg;

  factory _$Wind([void updates(WindBuilder b)]) =>
      (new WindBuilder()..update(updates)).build();

  _$Wind._({this.speed, this.deg}) : super._();

  @override
  Wind rebuild(void updates(WindBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  WindBuilder toBuilder() => new WindBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Wind && speed == other.speed && deg == other.deg;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, speed.hashCode), deg.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Wind')
          ..add('speed', speed)
          ..add('deg', deg))
        .toString();
  }
}

class WindBuilder implements Builder<Wind, WindBuilder> {
  _$Wind _$v;

  double _speed;
  double get speed => _$this._speed;
  set speed(double speed) => _$this._speed = speed;

  double _deg;
  double get deg => _$this._deg;
  set deg(double deg) => _$this._deg = deg;

  WindBuilder();

  WindBuilder get _$this {
    if (_$v != null) {
      _speed = _$v.speed;
      _deg = _$v.deg;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Wind other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Wind;
  }

  @override
  void update(void updates(WindBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Wind build() {
    final _$result = _$v ?? new _$Wind._(speed: speed, deg: deg);
    replace(_$result);
    return _$result;
  }
}

class _$Weather extends Weather {
  @override
  final int id;
  @override
  final String main;
  @override
  final String description;
  @override
  final String icon;

  factory _$Weather([void updates(WeatherBuilder b)]) =>
      (new WeatherBuilder()..update(updates)).build();

  _$Weather._({this.id, this.main, this.description, this.icon}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Weather', 'id');
    }
    if (main == null) {
      throw new BuiltValueNullFieldError('Weather', 'main');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('Weather', 'description');
    }
    if (icon == null) {
      throw new BuiltValueNullFieldError('Weather', 'icon');
    }
  }

  @override
  Weather rebuild(void updates(WeatherBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherBuilder toBuilder() => new WeatherBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Weather &&
        id == other.id &&
        main == other.main &&
        description == other.description &&
        icon == other.icon;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), main.hashCode), description.hashCode),
        icon.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Weather')
          ..add('id', id)
          ..add('main', main)
          ..add('description', description)
          ..add('icon', icon))
        .toString();
  }
}

class WeatherBuilder implements Builder<Weather, WeatherBuilder> {
  _$Weather _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _main;
  String get main => _$this._main;
  set main(String main) => _$this._main = main;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _icon;
  String get icon => _$this._icon;
  set icon(String icon) => _$this._icon = icon;

  WeatherBuilder();

  WeatherBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _main = _$v.main;
      _description = _$v.description;
      _icon = _$v.icon;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Weather other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Weather;
  }

  @override
  void update(void updates(WeatherBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Weather build() {
    final _$result = _$v ??
        new _$Weather._(
            id: id, main: main, description: description, icon: icon);
    replace(_$result);
    return _$result;
  }
}

class _$Clouds extends Clouds {
  @override
  final int all;

  factory _$Clouds([void updates(CloudsBuilder b)]) =>
      (new CloudsBuilder()..update(updates)).build();

  _$Clouds._({this.all}) : super._() {
    if (all == null) {
      throw new BuiltValueNullFieldError('Clouds', 'all');
    }
  }

  @override
  Clouds rebuild(void updates(CloudsBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  CloudsBuilder toBuilder() => new CloudsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Clouds && all == other.all;
  }

  @override
  int get hashCode {
    return $jf($jc(0, all.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Clouds')..add('all', all)).toString();
  }
}

class CloudsBuilder implements Builder<Clouds, CloudsBuilder> {
  _$Clouds _$v;

  int _all;
  int get all => _$this._all;
  set all(int all) => _$this._all = all;

  CloudsBuilder();

  CloudsBuilder get _$this {
    if (_$v != null) {
      _all = _$v.all;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Clouds other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Clouds;
  }

  @override
  void update(void updates(CloudsBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Clouds build() {
    final _$result = _$v ?? new _$Clouds._(all: all);
    replace(_$result);
    return _$result;
  }
}

class _$Rain extends Rain {
  @override
  final double oneHour;
  @override
  final double threeHours;

  factory _$Rain([void updates(RainBuilder b)]) =>
      (new RainBuilder()..update(updates)).build();

  _$Rain._({this.oneHour, this.threeHours}) : super._();

  @override
  Rain rebuild(void updates(RainBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  RainBuilder toBuilder() => new RainBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Rain &&
        oneHour == other.oneHour &&
        threeHours == other.threeHours;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, oneHour.hashCode), threeHours.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Rain')
          ..add('oneHour', oneHour)
          ..add('threeHours', threeHours))
        .toString();
  }
}

class RainBuilder implements Builder<Rain, RainBuilder> {
  _$Rain _$v;

  double _oneHour;
  double get oneHour => _$this._oneHour;
  set oneHour(double oneHour) => _$this._oneHour = oneHour;

  double _threeHours;
  double get threeHours => _$this._threeHours;
  set threeHours(double threeHours) => _$this._threeHours = threeHours;

  RainBuilder();

  RainBuilder get _$this {
    if (_$v != null) {
      _oneHour = _$v.oneHour;
      _threeHours = _$v.threeHours;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Rain other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Rain;
  }

  @override
  void update(void updates(RainBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Rain build() {
    final _$result =
        _$v ?? new _$Rain._(oneHour: oneHour, threeHours: threeHours);
    replace(_$result);
    return _$result;
  }
}

class _$Snow extends Snow {
  @override
  final double oneHour;
  @override
  final double threeHours;

  factory _$Snow([void updates(SnowBuilder b)]) =>
      (new SnowBuilder()..update(updates)).build();

  _$Snow._({this.oneHour, this.threeHours}) : super._();

  @override
  Snow rebuild(void updates(SnowBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SnowBuilder toBuilder() => new SnowBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Snow &&
        oneHour == other.oneHour &&
        threeHours == other.threeHours;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, oneHour.hashCode), threeHours.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Snow')
          ..add('oneHour', oneHour)
          ..add('threeHours', threeHours))
        .toString();
  }
}

class SnowBuilder implements Builder<Snow, SnowBuilder> {
  _$Snow _$v;

  double _oneHour;
  double get oneHour => _$this._oneHour;
  set oneHour(double oneHour) => _$this._oneHour = oneHour;

  double _threeHours;
  double get threeHours => _$this._threeHours;
  set threeHours(double threeHours) => _$this._threeHours = threeHours;

  SnowBuilder();

  SnowBuilder get _$this {
    if (_$v != null) {
      _oneHour = _$v.oneHour;
      _threeHours = _$v.threeHours;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Snow other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Snow;
  }

  @override
  void update(void updates(SnowBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Snow build() {
    final _$result =
        _$v ?? new _$Snow._(oneHour: oneHour, threeHours: threeHours);
    replace(_$result);
    return _$result;
  }
}

class _$Sys extends Sys {
  @override
  final String country;
  @override
  final int sunrise;
  @override
  final int sunset;

  factory _$Sys([void updates(SysBuilder b)]) =>
      (new SysBuilder()..update(updates)).build();

  _$Sys._({this.country, this.sunrise, this.sunset}) : super._();

  @override
  Sys rebuild(void updates(SysBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SysBuilder toBuilder() => new SysBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Sys &&
        country == other.country &&
        sunrise == other.sunrise &&
        sunset == other.sunset;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, country.hashCode), sunrise.hashCode), sunset.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Sys')
          ..add('country', country)
          ..add('sunrise', sunrise)
          ..add('sunset', sunset))
        .toString();
  }
}

class SysBuilder implements Builder<Sys, SysBuilder> {
  _$Sys _$v;

  String _country;
  String get country => _$this._country;
  set country(String country) => _$this._country = country;

  int _sunrise;
  int get sunrise => _$this._sunrise;
  set sunrise(int sunrise) => _$this._sunrise = sunrise;

  int _sunset;
  int get sunset => _$this._sunset;
  set sunset(int sunset) => _$this._sunset = sunset;

  SysBuilder();

  SysBuilder get _$this {
    if (_$v != null) {
      _country = _$v.country;
      _sunrise = _$v.sunrise;
      _sunset = _$v.sunset;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Sys other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Sys;
  }

  @override
  void update(void updates(SysBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Sys build() {
    final _$result =
        _$v ?? new _$Sys._(country: country, sunrise: sunrise, sunset: sunset);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
