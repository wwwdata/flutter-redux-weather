import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';

part 'weather_forecast.g.dart';

abstract class WeatherForecast
    implements Built<WeatherForecast, WeatherForecastBuilder> {
  factory WeatherForecast([updates(WeatherForecastBuilder b)]) =
      _$WeatherForecast;
  WeatherForecast._();

  int get cnt;
  City get city;
  BuiltList<ForecastItem> get list;

  static Serializer<WeatherForecast> get serializer =>
      _$weatherForecastSerializer;
}

abstract class City implements Built<City, CityBuilder> {
  factory City([updates(CityBuilder b)]) = _$City;
  City._();

  int get id;
  String get name;
  Coord get coord;
  String get country;

  static Serializer<City> get serializer => _$citySerializer;
}

abstract class ForecastItem
    implements Built<ForecastItem, ForecastItemBuilder> {
  factory ForecastItem([updates(ForecastItemBuilder b)]) = _$ForecastItem;
  ForecastItem._();

  int get dt;
  Main get main;
  BuiltList<Weather> get weather;
  Clouds get clouds;
  Wind get wind;
  @nullable
  Snow get snow;
  Sys get sys;

  static Serializer<ForecastItem> get serializer => _$forecastItemSerializer;
}
