// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_location.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SavedLocation> _$savedLocationSerializer =
    new _$SavedLocationSerializer();

class _$SavedLocationSerializer implements StructuredSerializer<SavedLocation> {
  @override
  final Iterable<Type> types = const [SavedLocation, _$SavedLocation];
  @override
  final String wireName = 'SavedLocation';

  @override
  Iterable serialize(Serializers serializers, SavedLocation object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'country',
      serializers.serialize(object.country,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SavedLocation deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SavedLocationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SavedLocation extends SavedLocation {
  @override
  final int id;
  @override
  final String name;
  @override
  final String country;

  factory _$SavedLocation([void updates(SavedLocationBuilder b)]) =>
      (new SavedLocationBuilder()..update(updates)).build();

  _$SavedLocation._({this.id, this.name, this.country}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('SavedLocation', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('SavedLocation', 'name');
    }
    if (country == null) {
      throw new BuiltValueNullFieldError('SavedLocation', 'country');
    }
  }

  @override
  SavedLocation rebuild(void updates(SavedLocationBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SavedLocationBuilder toBuilder() => new SavedLocationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SavedLocation &&
        id == other.id &&
        name == other.name &&
        country == other.country;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), country.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SavedLocation')
          ..add('id', id)
          ..add('name', name)
          ..add('country', country))
        .toString();
  }
}

class SavedLocationBuilder
    implements Builder<SavedLocation, SavedLocationBuilder> {
  _$SavedLocation _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _country;
  String get country => _$this._country;
  set country(String country) => _$this._country = country;

  SavedLocationBuilder();

  SavedLocationBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _country = _$v.country;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SavedLocation other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SavedLocation;
  }

  @override
  void update(void updates(SavedLocationBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$SavedLocation build() {
    final _$result =
        _$v ?? new _$SavedLocation._(id: id, name: name, country: country);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
