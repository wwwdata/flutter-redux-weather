// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_forecast.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherForecast> _$weatherForecastSerializer =
    new _$WeatherForecastSerializer();
Serializer<City> _$citySerializer = new _$CitySerializer();
Serializer<ForecastItem> _$forecastItemSerializer =
    new _$ForecastItemSerializer();

class _$WeatherForecastSerializer
    implements StructuredSerializer<WeatherForecast> {
  @override
  final Iterable<Type> types = const [WeatherForecast, _$WeatherForecast];
  @override
  final String wireName = 'WeatherForecast';

  @override
  Iterable serialize(Serializers serializers, WeatherForecast object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'cnt',
      serializers.serialize(object.cnt, specifiedType: const FullType(int)),
      'city',
      serializers.serialize(object.city, specifiedType: const FullType(City)),
      'list',
      serializers.serialize(object.list,
          specifiedType:
              const FullType(BuiltList, const [const FullType(ForecastItem)])),
    ];

    return result;
  }

  @override
  WeatherForecast deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherForecastBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cnt':
          result.cnt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'city':
          result.city.replace(serializers.deserialize(value,
              specifiedType: const FullType(City)) as City);
          break;
        case 'list':
          result.list.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ForecastItem)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$CitySerializer implements StructuredSerializer<City> {
  @override
  final Iterable<Type> types = const [City, _$City];
  @override
  final String wireName = 'City';

  @override
  Iterable serialize(Serializers serializers, City object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'coord',
      serializers.serialize(object.coord, specifiedType: const FullType(Coord)),
      'country',
      serializers.serialize(object.country,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  City deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CityBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coord':
          result.coord.replace(serializers.deserialize(value,
              specifiedType: const FullType(Coord)) as Coord);
          break;
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ForecastItemSerializer implements StructuredSerializer<ForecastItem> {
  @override
  final Iterable<Type> types = const [ForecastItem, _$ForecastItem];
  @override
  final String wireName = 'ForecastItem';

  @override
  Iterable serialize(Serializers serializers, ForecastItem object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'dt',
      serializers.serialize(object.dt, specifiedType: const FullType(int)),
      'main',
      serializers.serialize(object.main, specifiedType: const FullType(Main)),
      'weather',
      serializers.serialize(object.weather,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Weather)])),
      'clouds',
      serializers.serialize(object.clouds,
          specifiedType: const FullType(Clouds)),
      'wind',
      serializers.serialize(object.wind, specifiedType: const FullType(Wind)),
      'sys',
      serializers.serialize(object.sys, specifiedType: const FullType(Sys)),
    ];
    if (object.snow != null) {
      result
        ..add('snow')
        ..add(serializers.serialize(object.snow,
            specifiedType: const FullType(Snow)));
    }

    return result;
  }

  @override
  ForecastItem deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ForecastItemBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'dt':
          result.dt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'main':
          result.main.replace(serializers.deserialize(value,
              specifiedType: const FullType(Main)) as Main);
          break;
        case 'weather':
          result.weather.replace(serializers.deserialize(value,
              specifiedType: const FullType(
                  BuiltList, const [const FullType(Weather)])) as BuiltList);
          break;
        case 'clouds':
          result.clouds.replace(serializers.deserialize(value,
              specifiedType: const FullType(Clouds)) as Clouds);
          break;
        case 'wind':
          result.wind.replace(serializers.deserialize(value,
              specifiedType: const FullType(Wind)) as Wind);
          break;
        case 'snow':
          result.snow.replace(serializers.deserialize(value,
              specifiedType: const FullType(Snow)) as Snow);
          break;
        case 'sys':
          result.sys.replace(serializers.deserialize(value,
              specifiedType: const FullType(Sys)) as Sys);
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherForecast extends WeatherForecast {
  @override
  final int cnt;
  @override
  final City city;
  @override
  final BuiltList<ForecastItem> list;

  factory _$WeatherForecast([void updates(WeatherForecastBuilder b)]) =>
      (new WeatherForecastBuilder()..update(updates)).build();

  _$WeatherForecast._({this.cnt, this.city, this.list}) : super._() {
    if (cnt == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'cnt');
    }
    if (city == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'city');
    }
    if (list == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'list');
    }
  }

  @override
  WeatherForecast rebuild(void updates(WeatherForecastBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherForecastBuilder toBuilder() =>
      new WeatherForecastBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherForecast &&
        cnt == other.cnt &&
        city == other.city &&
        list == other.list;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, cnt.hashCode), city.hashCode), list.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherForecast')
          ..add('cnt', cnt)
          ..add('city', city)
          ..add('list', list))
        .toString();
  }
}

class WeatherForecastBuilder
    implements Builder<WeatherForecast, WeatherForecastBuilder> {
  _$WeatherForecast _$v;

  int _cnt;
  int get cnt => _$this._cnt;
  set cnt(int cnt) => _$this._cnt = cnt;

  CityBuilder _city;
  CityBuilder get city => _$this._city ??= new CityBuilder();
  set city(CityBuilder city) => _$this._city = city;

  ListBuilder<ForecastItem> _list;
  ListBuilder<ForecastItem> get list =>
      _$this._list ??= new ListBuilder<ForecastItem>();
  set list(ListBuilder<ForecastItem> list) => _$this._list = list;

  WeatherForecastBuilder();

  WeatherForecastBuilder get _$this {
    if (_$v != null) {
      _cnt = _$v.cnt;
      _city = _$v.city?.toBuilder();
      _list = _$v.list?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherForecast other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherForecast;
  }

  @override
  void update(void updates(WeatherForecastBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherForecast build() {
    _$WeatherForecast _$result;
    try {
      _$result = _$v ??
          new _$WeatherForecast._(
              cnt: cnt, city: city.build(), list: list.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'city';
        city.build();
        _$failedField = 'list';
        list.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherForecast', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$City extends City {
  @override
  final int id;
  @override
  final String name;
  @override
  final Coord coord;
  @override
  final String country;

  factory _$City([void updates(CityBuilder b)]) =>
      (new CityBuilder()..update(updates)).build();

  _$City._({this.id, this.name, this.coord, this.country}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('City', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('City', 'name');
    }
    if (coord == null) {
      throw new BuiltValueNullFieldError('City', 'coord');
    }
    if (country == null) {
      throw new BuiltValueNullFieldError('City', 'country');
    }
  }

  @override
  City rebuild(void updates(CityBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  CityBuilder toBuilder() => new CityBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is City &&
        id == other.id &&
        name == other.name &&
        coord == other.coord &&
        country == other.country;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc($jc(0, id.hashCode), name.hashCode), coord.hashCode),
        country.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('City')
          ..add('id', id)
          ..add('name', name)
          ..add('coord', coord)
          ..add('country', country))
        .toString();
  }
}

class CityBuilder implements Builder<City, CityBuilder> {
  _$City _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  CoordBuilder _coord;
  CoordBuilder get coord => _$this._coord ??= new CoordBuilder();
  set coord(CoordBuilder coord) => _$this._coord = coord;

  String _country;
  String get country => _$this._country;
  set country(String country) => _$this._country = country;

  CityBuilder();

  CityBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _coord = _$v.coord?.toBuilder();
      _country = _$v.country;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(City other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$City;
  }

  @override
  void update(void updates(CityBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$City build() {
    _$City _$result;
    try {
      _$result = _$v ??
          new _$City._(
              id: id, name: name, coord: coord.build(), country: country);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'coord';
        coord.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'City', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ForecastItem extends ForecastItem {
  @override
  final int dt;
  @override
  final Main main;
  @override
  final BuiltList<Weather> weather;
  @override
  final Clouds clouds;
  @override
  final Wind wind;
  @override
  final Snow snow;
  @override
  final Sys sys;

  factory _$ForecastItem([void updates(ForecastItemBuilder b)]) =>
      (new ForecastItemBuilder()..update(updates)).build();

  _$ForecastItem._(
      {this.dt,
      this.main,
      this.weather,
      this.clouds,
      this.wind,
      this.snow,
      this.sys})
      : super._() {
    if (dt == null) {
      throw new BuiltValueNullFieldError('ForecastItem', 'dt');
    }
    if (main == null) {
      throw new BuiltValueNullFieldError('ForecastItem', 'main');
    }
    if (weather == null) {
      throw new BuiltValueNullFieldError('ForecastItem', 'weather');
    }
    if (clouds == null) {
      throw new BuiltValueNullFieldError('ForecastItem', 'clouds');
    }
    if (wind == null) {
      throw new BuiltValueNullFieldError('ForecastItem', 'wind');
    }
    if (sys == null) {
      throw new BuiltValueNullFieldError('ForecastItem', 'sys');
    }
  }

  @override
  ForecastItem rebuild(void updates(ForecastItemBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ForecastItemBuilder toBuilder() => new ForecastItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ForecastItem &&
        dt == other.dt &&
        main == other.main &&
        weather == other.weather &&
        clouds == other.clouds &&
        wind == other.wind &&
        snow == other.snow &&
        sys == other.sys;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, dt.hashCode), main.hashCode),
                        weather.hashCode),
                    clouds.hashCode),
                wind.hashCode),
            snow.hashCode),
        sys.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ForecastItem')
          ..add('dt', dt)
          ..add('main', main)
          ..add('weather', weather)
          ..add('clouds', clouds)
          ..add('wind', wind)
          ..add('snow', snow)
          ..add('sys', sys))
        .toString();
  }
}

class ForecastItemBuilder
    implements Builder<ForecastItem, ForecastItemBuilder> {
  _$ForecastItem _$v;

  int _dt;
  int get dt => _$this._dt;
  set dt(int dt) => _$this._dt = dt;

  MainBuilder _main;
  MainBuilder get main => _$this._main ??= new MainBuilder();
  set main(MainBuilder main) => _$this._main = main;

  ListBuilder<Weather> _weather;
  ListBuilder<Weather> get weather =>
      _$this._weather ??= new ListBuilder<Weather>();
  set weather(ListBuilder<Weather> weather) => _$this._weather = weather;

  CloudsBuilder _clouds;
  CloudsBuilder get clouds => _$this._clouds ??= new CloudsBuilder();
  set clouds(CloudsBuilder clouds) => _$this._clouds = clouds;

  WindBuilder _wind;
  WindBuilder get wind => _$this._wind ??= new WindBuilder();
  set wind(WindBuilder wind) => _$this._wind = wind;

  SnowBuilder _snow;
  SnowBuilder get snow => _$this._snow ??= new SnowBuilder();
  set snow(SnowBuilder snow) => _$this._snow = snow;

  SysBuilder _sys;
  SysBuilder get sys => _$this._sys ??= new SysBuilder();
  set sys(SysBuilder sys) => _$this._sys = sys;

  ForecastItemBuilder();

  ForecastItemBuilder get _$this {
    if (_$v != null) {
      _dt = _$v.dt;
      _main = _$v.main?.toBuilder();
      _weather = _$v.weather?.toBuilder();
      _clouds = _$v.clouds?.toBuilder();
      _wind = _$v.wind?.toBuilder();
      _snow = _$v.snow?.toBuilder();
      _sys = _$v.sys?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ForecastItem other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ForecastItem;
  }

  @override
  void update(void updates(ForecastItemBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$ForecastItem build() {
    _$ForecastItem _$result;
    try {
      _$result = _$v ??
          new _$ForecastItem._(
              dt: dt,
              main: main.build(),
              weather: weather.build(),
              clouds: clouds.build(),
              wind: wind.build(),
              snow: _snow?.build(),
              sys: sys.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'main';
        main.build();
        _$failedField = 'weather';
        weather.build();
        _$failedField = 'clouds';
        clouds.build();
        _$failedField = 'wind';
        wind.build();
        _$failedField = 'snow';
        _snow?.build();
        _$failedField = 'sys';
        sys.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ForecastItem', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
