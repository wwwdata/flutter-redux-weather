import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_weather/credentials.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/redux/epics.dart';
import 'package:flutter_redux_weather/redux/reducer/app_reducer.dart';
import 'package:flutter_redux_weather/services/database.dart';
import 'package:flutter_redux_weather/services/weather_api.dart';
import 'package:flutter_redux_weather/ui/city_list_page.dart';
import 'package:flutter_redux_weather/ui/restore_gate.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_logging/redux_logging.dart';

void main() => runApp(AppContainer());

class AppContainer extends StatefulWidget {
  @override
  AppContainerState createState() {
    return new AppContainerState();
  }
}

class AppContainerState extends State<AppContainer> {
  final WeatherApi api = WeatherApi(apiKey);
  final DatabaseProvider database = DatabaseProvider();
  Epic<AppState> epics;
  Store<AppState> store;

  @override
  void initState() {
    epics = WeatherEpics(api, database);

    final List<Middleware<AppState>> middleware = <Middleware<AppState>>[
      EpicMiddleware<AppState>(epics)
    ];
    assert(() {
      middleware.add(
        LoggingMiddleware<AppState>.printer(
          formatter: onlyLogActionFormatter,
          // uncomment next line to log whole state as well
          //formatter: LoggingMiddleware.multiLineFormatter,
        ),
      );
      return true;
    }());
    store = Store<AppState>(
      appReducer,
      initialState: AppState.initial(),
      distinct: true,
      middleware: middleware,
    );
    super.initState();
  }

  @override
  void dispose() async {
    await database.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Redux Weather App',
        home: RestoreGate(
          child: CityListPage(),
        ),
      ),
    );
  }
}

String onlyLogActionFormatter<State>(
  State state,
  dynamic action,
  DateTime timestamp,
) {
  return '{Action: $action}';
}
