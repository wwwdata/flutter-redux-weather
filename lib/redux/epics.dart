import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/models/search_result.dart';
import 'package:flutter_redux_weather/models/weather_forecast.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/services/database.dart';
import 'package:flutter_redux_weather/services/weather_api.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class WeatherEpics implements EpicClass<AppState> {
  WeatherEpics(this.api, this.database);

  final WeatherApi api;
  final DatabaseProvider database;

  @override
  Stream<dynamic> call(Stream<dynamic> actions, EpicStore<AppState> store) {
    return Observable<dynamic>.merge(<Stream<dynamic>>[
      _handleRestore(actions, store),
      _handleSearchCity(actions, store),
      _handleUpdateWeatherData(actions, store),
      _handleLoadLocalWeather(actions, store),
      _handlePersistCity(actions, store),
    ]);
  }

  Stream<dynamic> _handleRestore(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return Observable<dynamic>(actions)
        .ofType(TypeToken<RestoreStateAction>())
        .flatMap((RestoreStateAction action) async* {
      final List<SavedLocation> locations = await database.getSavedLocations();
      yield ReceiveRestoredStateAction(locations);
    });
  }

  Stream<dynamic> _handleSearchCity(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return Observable<dynamic>(actions)
        .ofType(TypeToken<SearchCityAction>())
        .switchMap((SearchCityAction action) async* {
      try {
        final SearchResult result = await api.findCity(action.query);
        yield ReceiveSearchResultsAction(result, action.query);
      } catch (e) {
        yield ReceiveSearchResultsErrorAction(e.toString());
      }
    });
  }

  Stream<dynamic> _handlePersistCity(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return Observable<dynamic>(actions)
        .ofType(TypeToken<SaveWeatherLocationAction>())
        .flatMap((SaveWeatherLocationAction action) async* {
      try {
        await database.insertLocation(action.location);
      } catch (e) {
        print('database store failed $e');
        // todo: error handling
      }
    });
  }

  Stream<dynamic> _handleUpdateWeatherData(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return Observable<dynamic>.merge(<Observable<dynamic>>[
      Observable<dynamic>(actions)
          .ofType(TypeToken<TriggerUpdateWeatherDataAction>()),
      Observable<dynamic>(actions)
          .ofType(TypeToken<SaveWeatherLocationAction>()),
    ]).flatMap((dynamic action) async* {
      final List<Future<WeatherLocation>> locationFutures =
          <Future<WeatherLocation>>[];
      final List<Future<WeatherForecast>> forecastFutures =
          <Future<WeatherForecast>>[];

      try {
        for (SavedLocation savedLocation in store.state.data.savedCities) {
          locationFutures.add(api.queryLocation(id: savedLocation.id));
          forecastFutures.add(api.queryForecast(id: savedLocation.id));
        }

        final List<WeatherLocation> locationData =
            await Future.wait(locationFutures);
        final List<WeatherForecast> forecastData =
            await Future.wait(forecastFutures);
        yield ReceiveWeatherLocationDataAction(locationData);
        yield ReceiveWeatherForecastAction(forecastData);
      } catch (e, stack) {
        print('Updating weather data failed $e $stack');
        // TODO: error handling
      } finally {
        if (action is TriggerUpdateWeatherDataAction &&
            action.doneCallback != null) {
          action.doneCallback();
        }
      }
    });
  }

  Stream<dynamic> _handleLoadLocalWeather(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return Observable<dynamic>(actions)
        .ofType(TypeToken<LoadLocalWeatherAction>())
        .flatMap((dynamic action) async* {
      try {
        final WeatherLocation location =
            await api.queryLocation(coord: action.coord);
        final WeatherForecast forecast =
            await api.queryForecast(coord: action.coord);
        yield ReceiveLocalWeatherLocationAction(location);
        yield ReceiveLocalWeatherForecastAction(forecast);
      } catch (e) {
        print('loading local weather failed $e');
        // TODO: error handling
      }
    });
  }
}
