import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/models/search_result.dart';
import 'package:flutter_redux_weather/models/weather_forecast.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';

class RestoreStateAction {}

/// will be dispatched when the app restarts to restore saved cities
class ReceiveRestoredStateAction {
  ReceiveRestoredStateAction(this.locations);
  final List<SavedLocation> locations;
}

class SearchCityAction {
  SearchCityAction(this.query);
  final String query;
}

class ReceiveSearchResultsAction {
  ReceiveSearchResultsAction(this.result, this.query);
  final SearchResult result;
  final String query;
}

class ReceiveSearchResultsErrorAction {
  ReceiveSearchResultsErrorAction(this.error);
  final String error;
}

/// Add this location into the list so it appears on the home screen
class SaveWeatherLocationAction {
  SaveWeatherLocationAction(this.location);
  final SavedLocation location;
}

/// Trigger update of all stored weather locations
class TriggerUpdateWeatherDataAction {
  TriggerUpdateWeatherDataAction({this.doneCallback});

  /// optional callback that get's called when all loading is finished
  final Function() doneCallback;
}

class LoadLocalWeatherAction {
  LoadLocalWeatherAction(this.coord);
  Coord coord;
}

class ReceiveLocalWeatherLocationAction {
  ReceiveLocalWeatherLocationAction(this.weatherLocation);
  final WeatherLocation weatherLocation;
}

class ReceiveLocalWeatherForecastAction {
  ReceiveLocalWeatherForecastAction(this.weatherForecast);
  final WeatherForecast weatherForecast;
}

/// Receive data for many weather locations into state
class ReceiveWeatherLocationDataAction {
  ReceiveWeatherLocationDataAction(this.weatherLocations);
  final List<WeatherLocation> weatherLocations;
}

/// Receive forecast data for many weather locations
class ReceiveWeatherForecastAction {
  ReceiveWeatherForecastAction(this.weatherForecasts);
  final List<WeatherForecast> weatherForecasts;
}

/// Set loading state for all weather data update
class SetWeatherLoadingStateAction {
  SetWeatherLoadingStateAction({this.loading});
  final bool loading;
}
