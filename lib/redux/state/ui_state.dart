import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_redux_weather/models/search_result.dart';

part 'ui_state.g.dart';

abstract class UiState implements Built<UiState, UiStateBuilder> {
  factory UiState([updates(UiStateBuilder b)]) = _$UiState;
  factory UiState.initial() {
    return UiState((UiStateBuilder b) => b.searchQuery = '');
  }
  UiState._();

  @nullable
  String get searchError;
  @nullable
  String get searchQuery;
  @nullable
  SearchResult get searchResult;
  @nullable
  bool get loadingWeatherData;

  static Serializer<UiState> get serializer => _$uiStateSerializer;
}
