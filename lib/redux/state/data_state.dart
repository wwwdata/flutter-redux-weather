import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/models/weather_forecast.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';

part 'data_state.g.dart';

abstract class DataState implements Built<DataState, DataStateBuilder> {
  factory DataState([updates(DataStateBuilder b)]) = _$DataState;
  factory DataState.initial() {
    return DataState(
      (DataStateBuilder b) => b
        ..savedCities = SetBuilder<SavedLocation>(<SavedLocation>[])
        ..rehydrated = false,
    );
  }
  DataState._();

  bool get rehydrated;
  BuiltSet<SavedLocation> get savedCities;
  @nullable
  BuiltMap<int, WeatherLocation> get weatherDataByCityId;
  @nullable
  BuiltMap<int, WeatherForecast> get weatherForecastByCityId;
  @nullable
  WeatherLocation get localWeather;
  @nullable
  WeatherForecast get localForecast;

  static Serializer<DataState> get serializer => _$dataStateSerializer;
}
