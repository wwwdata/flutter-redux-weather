// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ui_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UiState> _$uiStateSerializer = new _$UiStateSerializer();

class _$UiStateSerializer implements StructuredSerializer<UiState> {
  @override
  final Iterable<Type> types = const [UiState, _$UiState];
  @override
  final String wireName = 'UiState';

  @override
  Iterable serialize(Serializers serializers, UiState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.searchError != null) {
      result
        ..add('searchError')
        ..add(serializers.serialize(object.searchError,
            specifiedType: const FullType(String)));
    }
    if (object.searchQuery != null) {
      result
        ..add('searchQuery')
        ..add(serializers.serialize(object.searchQuery,
            specifiedType: const FullType(String)));
    }
    if (object.searchResult != null) {
      result
        ..add('searchResult')
        ..add(serializers.serialize(object.searchResult,
            specifiedType: const FullType(SearchResult)));
    }
    if (object.loadingWeatherData != null) {
      result
        ..add('loadingWeatherData')
        ..add(serializers.serialize(object.loadingWeatherData,
            specifiedType: const FullType(bool)));
    }

    return result;
  }

  @override
  UiState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UiStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'searchError':
          result.searchError = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'searchQuery':
          result.searchQuery = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'searchResult':
          result.searchResult.replace(serializers.deserialize(value,
              specifiedType: const FullType(SearchResult)) as SearchResult);
          break;
        case 'loadingWeatherData':
          result.loadingWeatherData = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$UiState extends UiState {
  @override
  final String searchError;
  @override
  final String searchQuery;
  @override
  final SearchResult searchResult;
  @override
  final bool loadingWeatherData;

  factory _$UiState([void updates(UiStateBuilder b)]) =>
      (new UiStateBuilder()..update(updates)).build();

  _$UiState._(
      {this.searchError,
      this.searchQuery,
      this.searchResult,
      this.loadingWeatherData})
      : super._();

  @override
  UiState rebuild(void updates(UiStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UiStateBuilder toBuilder() => new UiStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UiState &&
        searchError == other.searchError &&
        searchQuery == other.searchQuery &&
        searchResult == other.searchResult &&
        loadingWeatherData == other.loadingWeatherData;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, searchError.hashCode), searchQuery.hashCode),
            searchResult.hashCode),
        loadingWeatherData.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UiState')
          ..add('searchError', searchError)
          ..add('searchQuery', searchQuery)
          ..add('searchResult', searchResult)
          ..add('loadingWeatherData', loadingWeatherData))
        .toString();
  }
}

class UiStateBuilder implements Builder<UiState, UiStateBuilder> {
  _$UiState _$v;

  String _searchError;
  String get searchError => _$this._searchError;
  set searchError(String searchError) => _$this._searchError = searchError;

  String _searchQuery;
  String get searchQuery => _$this._searchQuery;
  set searchQuery(String searchQuery) => _$this._searchQuery = searchQuery;

  SearchResultBuilder _searchResult;
  SearchResultBuilder get searchResult =>
      _$this._searchResult ??= new SearchResultBuilder();
  set searchResult(SearchResultBuilder searchResult) =>
      _$this._searchResult = searchResult;

  bool _loadingWeatherData;
  bool get loadingWeatherData => _$this._loadingWeatherData;
  set loadingWeatherData(bool loadingWeatherData) =>
      _$this._loadingWeatherData = loadingWeatherData;

  UiStateBuilder();

  UiStateBuilder get _$this {
    if (_$v != null) {
      _searchError = _$v.searchError;
      _searchQuery = _$v.searchQuery;
      _searchResult = _$v.searchResult?.toBuilder();
      _loadingWeatherData = _$v.loadingWeatherData;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UiState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UiState;
  }

  @override
  void update(void updates(UiStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UiState build() {
    _$UiState _$result;
    try {
      _$result = _$v ??
          new _$UiState._(
              searchError: searchError,
              searchQuery: searchQuery,
              searchResult: _searchResult?.build(),
              loadingWeatherData: loadingWeatherData);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'searchResult';
        _searchResult?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UiState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
