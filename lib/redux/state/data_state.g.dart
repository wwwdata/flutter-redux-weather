// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataState> _$dataStateSerializer = new _$DataStateSerializer();

class _$DataStateSerializer implements StructuredSerializer<DataState> {
  @override
  final Iterable<Type> types = const [DataState, _$DataState];
  @override
  final String wireName = 'DataState';

  @override
  Iterable serialize(Serializers serializers, DataState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'rehydrated',
      serializers.serialize(object.rehydrated,
          specifiedType: const FullType(bool)),
      'savedCities',
      serializers.serialize(object.savedCities,
          specifiedType:
              const FullType(BuiltSet, const [const FullType(SavedLocation)])),
    ];
    if (object.weatherDataByCityId != null) {
      result
        ..add('weatherDataByCityId')
        ..add(serializers.serialize(object.weatherDataByCityId,
            specifiedType: const FullType(BuiltMap,
                const [const FullType(int), const FullType(WeatherLocation)])));
    }
    if (object.weatherForecastByCityId != null) {
      result
        ..add('weatherForecastByCityId')
        ..add(serializers.serialize(object.weatherForecastByCityId,
            specifiedType: const FullType(BuiltMap,
                const [const FullType(int), const FullType(WeatherForecast)])));
    }
    if (object.localWeather != null) {
      result
        ..add('localWeather')
        ..add(serializers.serialize(object.localWeather,
            specifiedType: const FullType(WeatherLocation)));
    }
    if (object.localForecast != null) {
      result
        ..add('localForecast')
        ..add(serializers.serialize(object.localForecast,
            specifiedType: const FullType(WeatherForecast)));
    }

    return result;
  }

  @override
  DataState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'rehydrated':
          result.rehydrated = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'savedCities':
          result.savedCities.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltSet, const [const FullType(SavedLocation)]))
              as BuiltSet);
          break;
        case 'weatherDataByCityId':
          result.weatherDataByCityId.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(int),
                const FullType(WeatherLocation)
              ])) as BuiltMap);
          break;
        case 'weatherForecastByCityId':
          result.weatherForecastByCityId.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(int),
                const FullType(WeatherForecast)
              ])) as BuiltMap);
          break;
        case 'localWeather':
          result.localWeather.replace(serializers.deserialize(value,
                  specifiedType: const FullType(WeatherLocation))
              as WeatherLocation);
          break;
        case 'localForecast':
          result.localForecast.replace(serializers.deserialize(value,
                  specifiedType: const FullType(WeatherForecast))
              as WeatherForecast);
          break;
      }
    }

    return result.build();
  }
}

class _$DataState extends DataState {
  @override
  final bool rehydrated;
  @override
  final BuiltSet<SavedLocation> savedCities;
  @override
  final BuiltMap<int, WeatherLocation> weatherDataByCityId;
  @override
  final BuiltMap<int, WeatherForecast> weatherForecastByCityId;
  @override
  final WeatherLocation localWeather;
  @override
  final WeatherForecast localForecast;

  factory _$DataState([void updates(DataStateBuilder b)]) =>
      (new DataStateBuilder()..update(updates)).build();

  _$DataState._(
      {this.rehydrated,
      this.savedCities,
      this.weatherDataByCityId,
      this.weatherForecastByCityId,
      this.localWeather,
      this.localForecast})
      : super._() {
    if (rehydrated == null) {
      throw new BuiltValueNullFieldError('DataState', 'rehydrated');
    }
    if (savedCities == null) {
      throw new BuiltValueNullFieldError('DataState', 'savedCities');
    }
  }

  @override
  DataState rebuild(void updates(DataStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  DataStateBuilder toBuilder() => new DataStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataState &&
        rehydrated == other.rehydrated &&
        savedCities == other.savedCities &&
        weatherDataByCityId == other.weatherDataByCityId &&
        weatherForecastByCityId == other.weatherForecastByCityId &&
        localWeather == other.localWeather &&
        localForecast == other.localForecast;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, rehydrated.hashCode), savedCities.hashCode),
                    weatherDataByCityId.hashCode),
                weatherForecastByCityId.hashCode),
            localWeather.hashCode),
        localForecast.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataState')
          ..add('rehydrated', rehydrated)
          ..add('savedCities', savedCities)
          ..add('weatherDataByCityId', weatherDataByCityId)
          ..add('weatherForecastByCityId', weatherForecastByCityId)
          ..add('localWeather', localWeather)
          ..add('localForecast', localForecast))
        .toString();
  }
}

class DataStateBuilder implements Builder<DataState, DataStateBuilder> {
  _$DataState _$v;

  bool _rehydrated;
  bool get rehydrated => _$this._rehydrated;
  set rehydrated(bool rehydrated) => _$this._rehydrated = rehydrated;

  SetBuilder<SavedLocation> _savedCities;
  SetBuilder<SavedLocation> get savedCities =>
      _$this._savedCities ??= new SetBuilder<SavedLocation>();
  set savedCities(SetBuilder<SavedLocation> savedCities) =>
      _$this._savedCities = savedCities;

  MapBuilder<int, WeatherLocation> _weatherDataByCityId;
  MapBuilder<int, WeatherLocation> get weatherDataByCityId =>
      _$this._weatherDataByCityId ??= new MapBuilder<int, WeatherLocation>();
  set weatherDataByCityId(
          MapBuilder<int, WeatherLocation> weatherDataByCityId) =>
      _$this._weatherDataByCityId = weatherDataByCityId;

  MapBuilder<int, WeatherForecast> _weatherForecastByCityId;
  MapBuilder<int, WeatherForecast> get weatherForecastByCityId =>
      _$this._weatherForecastByCityId ??=
          new MapBuilder<int, WeatherForecast>();
  set weatherForecastByCityId(
          MapBuilder<int, WeatherForecast> weatherForecastByCityId) =>
      _$this._weatherForecastByCityId = weatherForecastByCityId;

  WeatherLocationBuilder _localWeather;
  WeatherLocationBuilder get localWeather =>
      _$this._localWeather ??= new WeatherLocationBuilder();
  set localWeather(WeatherLocationBuilder localWeather) =>
      _$this._localWeather = localWeather;

  WeatherForecastBuilder _localForecast;
  WeatherForecastBuilder get localForecast =>
      _$this._localForecast ??= new WeatherForecastBuilder();
  set localForecast(WeatherForecastBuilder localForecast) =>
      _$this._localForecast = localForecast;

  DataStateBuilder();

  DataStateBuilder get _$this {
    if (_$v != null) {
      _rehydrated = _$v.rehydrated;
      _savedCities = _$v.savedCities?.toBuilder();
      _weatherDataByCityId = _$v.weatherDataByCityId?.toBuilder();
      _weatherForecastByCityId = _$v.weatherForecastByCityId?.toBuilder();
      _localWeather = _$v.localWeather?.toBuilder();
      _localForecast = _$v.localForecast?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataState;
  }

  @override
  void update(void updates(DataStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$DataState build() {
    _$DataState _$result;
    try {
      _$result = _$v ??
          new _$DataState._(
              rehydrated: rehydrated,
              savedCities: savedCities.build(),
              weatherDataByCityId: _weatherDataByCityId?.build(),
              weatherForecastByCityId: _weatherForecastByCityId?.build(),
              localWeather: _localWeather?.build(),
              localForecast: _localForecast?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'savedCities';
        savedCities.build();
        _$failedField = 'weatherDataByCityId';
        _weatherDataByCityId?.build();
        _$failedField = 'weatherForecastByCityId';
        _weatherForecastByCityId?.build();
        _$failedField = 'localWeather';
        _localWeather?.build();
        _$failedField = 'localForecast';
        _localForecast?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
