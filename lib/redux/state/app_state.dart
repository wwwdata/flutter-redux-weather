import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_redux_weather/redux/state/data_state.dart';
import 'package:flutter_redux_weather/redux/state/ui_state.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  factory AppState([updates(AppStateBuilder b)]) = _$AppState;
  factory AppState.initial() {
    return AppState(
      (AppStateBuilder b) => b
        ..data = DataState.initial().toBuilder()
        ..ui = UiState.initial().toBuilder(),
    );
  }
  AppState._();

  DataState get data;
  UiState get ui;

  static Serializer<AppState> get serializer => _$appStateSerializer;
}
