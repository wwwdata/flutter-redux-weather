import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/redux/reducer/data_reducer.dart';
import 'package:flutter_redux_weather/redux/reducer/ui_reducer.dart';

AppState appReducer(AppState state, dynamic action) {
  if (state == null) {
    return AppState.initial();
  }

  return AppState(
    (AppStateBuilder b) => b
      ..data = dataReducer(state.data, action).toBuilder()
      ..ui = uiReducer(state.ui, action).toBuilder(),
  );
}
