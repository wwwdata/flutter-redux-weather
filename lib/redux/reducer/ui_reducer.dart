import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/ui_state.dart';
import 'package:redux/redux.dart';

final Reducer<UiState> uiReducer = combineReducers<UiState>(<Reducer<UiState>>[
  TypedReducer<UiState, ReceiveSearchResultsErrorAction>(_searchResultsError),
  TypedReducer<UiState, ReceiveSearchResultsAction>(_searchResults),
  TypedReducer<UiState, SetWeatherLoadingStateAction>(_loadingWeatherState),
]);

UiState _searchResultsError(
    UiState state, ReceiveSearchResultsErrorAction action) {
  return state.rebuild((UiStateBuilder b) => b.searchError = action.error);
}

UiState _searchResults(UiState state, ReceiveSearchResultsAction action) {
  return state.rebuild(
    (UiStateBuilder b) => b
      ..searchResult = action.result.toBuilder()
      ..searchQuery = action.query
      ..searchError = null,
  );
}

UiState _loadingWeatherState(
    UiState state, SetWeatherLoadingStateAction action) {
  return state
      .rebuild((UiStateBuilder b) => b.loadingWeatherData = action.loading);
}
