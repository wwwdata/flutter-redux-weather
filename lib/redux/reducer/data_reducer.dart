import 'package:flutter_redux_weather/models/weather_forecast.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/data_state.dart';
import 'package:redux/redux.dart';

final Reducer<DataState> dataReducer =
    combineReducers<DataState>(<Reducer<DataState>>[
  TypedReducer<DataState, ReceiveRestoredStateAction>(_restore),
  TypedReducer<DataState, SaveWeatherLocationAction>(_saveCity),
  TypedReducer<DataState, ReceiveWeatherLocationDataAction>(
      _weatherLocationData),
  TypedReducer<DataState, ReceiveWeatherForecastAction>(_weatherForecast),
  TypedReducer<DataState, ReceiveLocalWeatherLocationAction>(_localWeather),
  TypedReducer<DataState, ReceiveLocalWeatherForecastAction>(_localForecast),
]);

DataState _restore(DataState state, ReceiveRestoredStateAction action) {
  return state.rebuild(
    (DataStateBuilder b) {
      // could also be empty when the app is fresh
      if (action.locations != null) {
        b.savedCities.addAll(action.locations);
      }
      b.rehydrated = true;
    },
  );
}

DataState _saveCity(DataState state, SaveWeatherLocationAction action) {
  return state
      .rebuild((DataStateBuilder b) => b..savedCities.add(action.location));
}

DataState _weatherLocationData(
    DataState state, ReceiveWeatherLocationDataAction action) {
  return state.rebuild((DataStateBuilder b) {
    action.weatherLocations.forEach((WeatherLocation location) =>
        b.weatherDataByCityId[location.id] = location);
  });
}

DataState _weatherForecast(
    DataState state, ReceiveWeatherForecastAction action) {
  return state.rebuild((DataStateBuilder b) {
    action.weatherForecasts.forEach((WeatherForecast location) =>
        b.weatherForecastByCityId[location.city.id] = location);
  });
}

DataState _localWeather(
    DataState state, ReceiveLocalWeatherLocationAction action) {
  return state.rebuild((DataStateBuilder b) =>
      b.localWeather = action.weatherLocation.toBuilder());
}

DataState _localForecast(
    DataState state, ReceiveLocalWeatherForecastAction action) {
  return state.rebuild((DataStateBuilder b) =>
      b.localForecast = action.weatherForecast.toBuilder());
}
