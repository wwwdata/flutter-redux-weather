import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_weather/models/weather_forecast.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/ui/elements/weather_icon.dart';
import 'package:redux/redux.dart';
import 'package:intl/intl.dart';

class CityDetailPage extends StatelessWidget {
  const CityDetailPage({Key key, this.id, this.isLocal}) : super(key: key);
  final int id;
  final bool isLocal;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _CityDetailPageViewModel>(
      distinct: true,
      converter: (Store<AppState> store) =>
          _CityDetailPageViewModel.fromStore(store, id: id, isLocal: isLocal),
      builder: (BuildContext context, _CityDetailPageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
                '${viewModel.forecast.city.name}, ${viewModel.forecast.city.country}'),
          ),
          body: _buildContent(context, viewModel),
        );
      },
    );
  }

  Widget _buildContent(
      BuildContext context, _CityDetailPageViewModel viewModel) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(viewModel.current.weather.first.description,
                    style: Theme.of(context).textTheme.headline),
                Text(viewModel.current.main.temp.toString(),
                    style: Theme.of(context).textTheme.display3),
                Text(
                    'min: ${viewModel.current.main.tempMin}, max: ${viewModel.current.main.tempMax}'),
                Text('Forecast:', style: Theme.of(context).textTheme.display1),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: _buildForecast(context, viewModel.forecast.list.toList()),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForecast(BuildContext context, List<ForecastItem> forecasts) {
    // group by day
    final List<List<ForecastItem>> foreCastsByDay = <List<ForecastItem>>[];

    int day;
    List<ForecastItem> currentList = <ForecastItem>[];

    for (ForecastItem item in forecasts) {
      final DateTime time = DateTime.fromMillisecondsSinceEpoch(item.dt * 1000);

      day ??= time.day;
      if (time.day == day) {
        // same day, add to list
        currentList.add(item);
      } else {
        // next day, start new list
        foreCastsByDay.add(currentList);
        day = time.day;
        currentList = <ForecastItem>[item];
      }
    }

    // add last list
    foreCastsByDay.add(currentList);

    return ListView.separated(
      itemCount: foreCastsByDay.length,
      itemBuilder: (BuildContext context, int index) =>
          _buildDayForecasts(context, foreCastsByDay[index]),
      separatorBuilder: (BuildContext context, int index) => Divider(),
    );
  }

  Widget _buildDayForecasts(
      BuildContext context, List<ForecastItem> forecasts) {
    final DateTime time =
        DateTime.fromMillisecondsSinceEpoch(forecasts.first.dt * 1000);
    final DateFormat dayFormatter = DateFormat('EEEE');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(dayFormatter.format(time),
              style: Theme.of(context).textTheme.title),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
              children: forecasts.map(
            (ForecastItem item) {
              final DateTime time =
                  DateTime.fromMillisecondsSinceEpoch(item.dt * 1000);
              final DateFormat timeFormatter = DateFormat('HH:mm');

              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: Column(
                  children: <Widget>[
                    Text(timeFormatter.format(time),
                        style: Theme.of(context).textTheme.caption),
                    Text(item.main.temp.toString(),
                        style: Theme.of(context).textTheme.subhead),
                    WeatherIcon(icon: item.weather.first.icon),
                  ],
                ),
              );
            },
          ).toList()),
        ),
      ],
    );
  }
}

class _CityDetailPageViewModel {
  _CityDetailPageViewModel({this.current, this.forecast});
  factory _CityDetailPageViewModel.fromStore(Store<AppState> store,
      {int id, bool isLocal}) {
    return _CityDetailPageViewModel(
      current: isLocal == true
          ? store.state.data.localWeather
          : store.state.data.weatherDataByCityId[id],
      forecast: isLocal == true
          ? store.state.data.localForecast
          : store.state.data.weatherForecastByCityId[id],
    );
  }
  final WeatherLocation current;
  final WeatherForecast forecast;

  @override
  int get hashCode => current.hashCode ^ forecast.hashCode;

  @override
  bool operator ==(dynamic other) =>
      other is _CityDetailPageViewModel &&
      other.current == current &&
      other.forecast == forecast;
}
