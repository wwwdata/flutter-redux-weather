import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/models/search_result.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/ui/elements/weather_location_tile.dart';
import 'package:redux/redux.dart';

class CitySearchPage extends StatefulWidget {
  @override
  CitySearchPageState createState() {
    return new CitySearchPageState();
  }
}

class CitySearchPageState extends State<CitySearchPage> {
  TextEditingController _textController;
  String error;

  @override
  void initState() {
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _CitySearchPageViewModel>(
      distinct: true,
      converter: (Store<AppState> store) =>
          _CitySearchPageViewModel.fromStore(store),
      onInitialBuild: (_CitySearchPageViewModel viewModel) {
        _textController.text = viewModel.searchQuery;
      },
      builder: (BuildContext context, _CitySearchPageViewModel viewModel) =>
          Scaffold(
            appBar: AppBar(
              title: Text('Add City'),
            ),
            body: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _textController,
                    autocorrect: false,
                    decoration: InputDecoration(
                      suffix: InkWell(
                        child: Icon(
                          Icons.cancel,
                          size: Theme.of(context).textTheme.body1.fontSize,
                          color: Theme.of(context).hintColor,
                        ),
                        onTap: () => _textController.clear(),
                      ),
                      labelText: 'City name',
                      errorText: error ?? viewModel.errorMessage,
                    ),
                    onChanged: (String value) => _onChanged(viewModel, value),
                  ),
                ),
                Expanded(
                  child: ListView.separated(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    itemCount: viewModel.result?.count ?? 0,
                    itemBuilder: (BuildContext context, int index) {
                      final WeatherLocation location =
                          viewModel.result.list[index];
                      return WeatherLocationTile(
                        weatherLocation: location,
                        onPress: () => _saveCity(
                            context, viewModel, location.asSavedLocation()),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(indent: 68.0);
                    },
                  ),
                )
              ],
            ),
          ),
    );
  }

  void _onChanged(_CitySearchPageViewModel viewModel, String newValue) {
    if (newValue.length < 3) {
      setState(() {
        error = 'Type at least 3 chars';
      });
    } else {
      setState(() {
        error = null;
      });
      viewModel.submitSearch(newValue);
    }
  }

  void _saveCity(BuildContext context, _CitySearchPageViewModel viewModel,
      SavedLocation savedLocation) {
    viewModel.saveCity(savedLocation);
    Navigator.of(context).pop();
  }
}

class _CitySearchPageViewModel {
  _CitySearchPageViewModel({
    this.submitSearch,
    this.saveCity,
    this.errorMessage,
    this.result,
    this.searchQuery,
  });

  factory _CitySearchPageViewModel.fromStore(Store<AppState> store) {
    return _CitySearchPageViewModel(
      submitSearch: (String query) => store.dispatch(
            SearchCityAction(query),
          ),
      saveCity: (SavedLocation location) =>
          store.dispatch(SaveWeatherLocationAction(location)),
      errorMessage: store.state.ui.searchError,
      result: store.state.ui.searchResult,
      searchQuery: store.state.ui.searchQuery,
    );
  }

  final Function(String query) submitSearch;
  final Function(SavedLocation location) saveCity;
  final String errorMessage;
  final SearchResult result;
  final String searchQuery;

  @override
  int get hashCode =>
      errorMessage.hashCode ^ result.hashCode ^ searchQuery.hashCode;

  @override
  bool operator ==(dynamic other) {
    return other is _CitySearchPageViewModel &&
        other.errorMessage == errorMessage &&
        other.result == result &&
        other.searchQuery == searchQuery;
  }
}
