import 'package:flutter/material.dart';

class WeatherIcon extends StatelessWidget {
  const WeatherIcon({@required this.icon, Key key}) : super(key: key);
  final String icon;

  @override
  Widget build(BuildContext context) {
    return Image.network(
      _getIconUrl(icon),
    );
  }

  String _getIconUrl(String icon) {
    return 'http://openweathermap.org/img/w/$icon.png';
  }
}
