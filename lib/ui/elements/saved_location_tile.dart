import 'package:flutter/material.dart';
import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/ui/elements/weather_location_tile.dart';

/// This is a placeholder tile that can be swapped with [WeatherLocationTile]
/// when the weather data was loaded for the city
class SavedLocationTile extends StatelessWidget {
  const SavedLocationTile({Key key, this.savedLocation}) : super(key: key);
  final SavedLocation savedLocation;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: AspectRatio(
                    aspectRatio: 1.0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          '${savedLocation.name}, ${savedLocation.country}',
                          style: Theme.of(context).textTheme.title,
                        ),
                      ),
                      Text(
                        '',
                        style: Theme.of(context).textTheme.subhead,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
