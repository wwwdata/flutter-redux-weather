import 'package:flutter/material.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/ui/elements/weather_icon.dart';
import 'package:intl/intl.dart';

class WeatherLocationTile extends StatelessWidget {
  const WeatherLocationTile(
      {@required this.weatherLocation, this.onPress, Key key})
      : super(key: key);
  final WeatherLocation weatherLocation;
  final VoidCallback onPress;

  @override
  Widget build(BuildContext context) {
    final DateTime measureDate =
        DateTime.fromMillisecondsSinceEpoch(weatherLocation.dt * 1000);

    final DateFormat timeFormatter = DateFormat('HH:mm');
    return InkWell(
      onTap: onPress,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        height: 50.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: AspectRatio(
                        aspectRatio: 1.0,
                        child: WeatherIcon(
                            icon: weatherLocation.weather.first.icon)),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            '${weatherLocation.name}, ${weatherLocation.sys.country}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.title,
                          ),
                        ),
                        Text(
                          timeFormatter.format(measureDate),
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Text(
              '${weatherLocation.main.temp.toString()}°',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
    );
  }
}
