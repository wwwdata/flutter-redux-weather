import 'dart:async';
import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/ui/city_detail_page.dart';
import 'package:flutter_redux_weather/ui/city_search_page.dart';
import 'package:flutter_redux_weather/ui/elements/saved_location_tile.dart';
import 'package:flutter_redux_weather/ui/elements/weather_location_tile.dart';
import 'package:redux/redux.dart';

class CityListPage extends StatefulWidget {
  @override
  CityListPageState createState() {
    return new CityListPageState();
  }
}

class CityListPageState extends State<CityListPage> {
  static const MethodChannel platform =
      const MethodChannel('example.flutter_redux_weather/location');
  Coord localCoords;
  String localCoordsError;

  Future<void> _getLocalCoords() async {
    try {
      final String coordsJson = await platform.invokeMethod('getCoords');
      final Map<String, dynamic> coords = json.decode(coordsJson);
      setState(() {
        localCoords = Coord(
          (CoordBuilder b) => b
            ..lat = coords['lat']
            ..lon = coords['lon'],
        );
        localCoordsError = null;
        StoreProvider.of<AppState>(context)
            .dispatch(LoadLocalWeatherAction(localCoords));
      });
    } on PlatformException catch (e) {
      setState(() {
        localCoordsError = e.message;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getLocalCoords();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cities'),
      ),
      body: Scrollbar(
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: _buildContent(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(
              context,
              MaterialPageRoute<CitySearchPage>(
                builder: (BuildContext context) => CitySearchPage(),
              ),
            ),
      ),
    );
  }

  Widget _buildContent() {
    return StoreConnector<AppState, _CityListPageViewModel>(
      distinct: true,
      onInit: (Store<AppState> store) =>
          store.dispatch(TriggerUpdateWeatherDataAction()),
      converter: (Store<AppState> store) =>
          _CityListPageViewModel.fromStore(store),
      builder: (BuildContext context, _CityListPageViewModel viewModel) =>
          RefreshIndicator(
            onRefresh: () async {
              await _getLocalCoords();
              await viewModel.refresh();
            },
            child: ListView.separated(
              itemCount: viewModel.savedLocations.length + 1,
              itemBuilder: (BuildContext context, int index) {
                WeatherLocation weatherLocation;
                SavedLocation savedLocation;
                final bool isLocal = index == 0;

                if (isLocal) {
                  weatherLocation = viewModel.localWeather;
                } else {
                  savedLocation = viewModel.savedLocations.elementAt(index - 1);
                  weatherLocation =
                      viewModel.weatherDataByCityId[savedLocation.id];
                }

                return AnimatedSwitcher(
                  duration: const Duration(milliseconds: 300),
                  child: weatherLocation != null
                      ? WeatherLocationTile(
                          weatherLocation: weatherLocation,
                          onPress: () => Navigator.of(context).push(
                                MaterialPageRoute<CityDetailPage>(
                                  builder: (BuildContext context) =>
                                      CityDetailPage(
                                        id: weatherLocation.id,
                                        isLocal: isLocal,
                                      ),
                                ),
                              ),
                        )
                      : index == 0
                          ? Text(localCoordsError ?? 'Loading local weather...',
                              style: Theme.of(context).textTheme.headline)
                          : SavedLocationTile(savedLocation: savedLocation),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Divider(indent: 68.0);
              },
            ),
          ),
    );
  }
}

class _CityListPageViewModel {
  _CityListPageViewModel({
    this.savedLocations,
    this.weatherDataByCityId,
    this.localWeather,
    this.refresh,
  });
  factory _CityListPageViewModel.fromStore(Store<AppState> store) {
    return _CityListPageViewModel(
        savedLocations: store.state.data.savedCities,
        weatherDataByCityId: store.state.data.weatherDataByCityId ??
            BuiltMap<int, WeatherLocation>(<int, WeatherLocation>{}),
        localWeather: store.state.data.localWeather,
        refresh: () async {
          final Completer<void> completer = Completer<void>();
          store.dispatch(TriggerUpdateWeatherDataAction(
              doneCallback: () => completer.complete()));
          return completer.future;
        });
  }

  final BuiltSet<SavedLocation> savedLocations;
  final BuiltMap<int, WeatherLocation> weatherDataByCityId;
  final WeatherLocation localWeather;
  final RefreshCallback refresh;

  @override
  int get hashCode =>
      savedLocations.hashCode ^
      weatherDataByCityId.hashCode ^
      localWeather.hashCode;

  @override
  bool operator ==(dynamic other) {
    return other is _CityListPageViewModel &&
        other.savedLocations == savedLocations &&
        other.weatherDataByCityId == weatherDataByCityId &&
        other.localWeather == localWeather;
  }
}
