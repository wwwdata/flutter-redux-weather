import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:redux/redux.dart';

/// Will dispatch the restore state action and render a placeholder as long
/// as it is not restored
class RestoreGate extends StatelessWidget {
  const RestoreGate({Key key, this.child}) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, bool>(
      onInit: (Store<AppState> store) => store.dispatch(RestoreStateAction()),
      converter: (Store<AppState> store) => store.state.data.rehydrated,
      distinct: true,
      builder: (BuildContext context, bool restored) {
        return AnimatedSwitcher(
          duration: const Duration(milliseconds: 300),
          child: restored == true
              ? child
              : Scaffold(
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
        );
      },
    );
  }
}
