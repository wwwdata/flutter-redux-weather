#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import <CoreLocation/CoreLocation.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GeneratedPluginRegistrant registerWithRegistry:self];
    // Override point for customization after application launch.
    FlutterViewController* controller = (FlutterViewController*)self.window.rootViewController;
    
    FlutterMethodChannel* coordsChannel = [FlutterMethodChannel
                                           methodChannelWithName:@"example.flutter_redux_weather/location"
                                           binaryMessenger:controller];
    
    CLLocationManager* locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    [coordsChannel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
        if ([@"getCoords" isEqualToString:call.method]) {
            if (CLLocationManager.authorizationStatus != kCLAuthorizationStatusAuthorizedWhenInUse) {
                result([FlutterError errorWithCode:@"UNAVAILABLE" message:@"Location permission not set" details:nil]);
            }
            result([NSString stringWithFormat:@"{\"lat\": %f, \"lon\": %f}",
                    locationManager.location.coordinate.latitude,
                    locationManager.location.coordinate.longitude
                    ]);
        } else {
            result(FlutterMethodNotImplemented);
        }
    }];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
