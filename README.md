# flutter_redux_weather

Example app for my flutter talk in the Munich Flutter Meetup Group.

## Tests

Database integration tests:

```
flutter drive --target=test_driver/database.dart
```

Integration tests:

```
flutter drive --target=test_driver/integration.dart
```
