import 'package:test/test.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'examples_test.g.dart';

void main() {
  test('simple object equality', () async {
    final SimpleObject objA = SimpleObject('test');
    final SimpleObject objB = SimpleObject('test');

    expect(objA, objB);
  });

  test('build value object equality', () async {
    final SimpleBuiltValueObject objA = SimpleBuiltValueObject(
        (SimpleBuiltValueObjectBuilder b) => b.foo = 'test');
    final SimpleBuiltValueObject objB = SimpleBuiltValueObject(
        (SimpleBuiltValueObjectBuilder b) => b.foo = 'test');

    expect(objA, objB);
  });
}

class SimpleObject {
  SimpleObject(this.foo);
  final String foo;

  @override
  int get hashCode => foo.hashCode;

  @override
  bool operator ==(dynamic other) => other is SimpleObject && other.foo == foo;
}

abstract class SimpleBuiltValueObject
    implements Built<SimpleBuiltValueObject, SimpleBuiltValueObjectBuilder> {
  factory SimpleBuiltValueObject([updates(SimpleBuiltValueObjectBuilder b)]) =
      _$SimpleBuiltValueObject;
  SimpleBuiltValueObject._();

  String get foo;

  static Serializer<SimpleBuiltValueObject> get serializer =>
      _$simpleBuiltValueObjectSerializer;
}
