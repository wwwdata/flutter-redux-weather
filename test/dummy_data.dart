import 'package:built_collection/built_collection.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';

final WeatherLocation dummyWeatherLocation = WeatherLocation(
  (WeatherLocationBuilder b) => b
    ..id = 123
    ..name = 'Test'
    ..dt = 123
    ..coord.lon = 0.0
    ..coord.lat = 0.0
    ..weather = ListBuilder<Weather>(<Weather>[
      Weather((WeatherBuilder b) => b
        ..id = 0
        ..main = 'Main'
        ..description = 'Unicorns raining down'
        ..icon = '01d'),
    ])
    ..main.temp = 3.0
    ..main.pressure = 500.0
    ..main.humidity = 50
    ..main.tempMin = -100.0
    ..main.tempMax = 200.0
    ..wind.speed = 50.0
    ..wind.deg = 2.0
    ..clouds.all = 100
    ..sys.country = 'DE',
);
