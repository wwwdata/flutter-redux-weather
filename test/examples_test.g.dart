// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'examples_test.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SimpleBuiltValueObject> _$simpleBuiltValueObjectSerializer =
    new _$SimpleBuiltValueObjectSerializer();

class _$SimpleBuiltValueObjectSerializer
    implements StructuredSerializer<SimpleBuiltValueObject> {
  @override
  final Iterable<Type> types = const [
    SimpleBuiltValueObject,
    _$SimpleBuiltValueObject
  ];
  @override
  final String wireName = 'SimpleBuiltValueObject';

  @override
  Iterable serialize(Serializers serializers, SimpleBuiltValueObject object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'foo',
      serializers.serialize(object.foo, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SimpleBuiltValueObject deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SimpleBuiltValueObjectBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'foo':
          result.foo = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SimpleBuiltValueObject extends SimpleBuiltValueObject {
  @override
  final String foo;

  factory _$SimpleBuiltValueObject(
          [void updates(SimpleBuiltValueObjectBuilder b)]) =>
      (new SimpleBuiltValueObjectBuilder()..update(updates)).build();

  _$SimpleBuiltValueObject._({this.foo}) : super._() {
    if (foo == null) {
      throw new BuiltValueNullFieldError('SimpleBuiltValueObject', 'foo');
    }
  }

  @override
  SimpleBuiltValueObject rebuild(
          void updates(SimpleBuiltValueObjectBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SimpleBuiltValueObjectBuilder toBuilder() =>
      new SimpleBuiltValueObjectBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SimpleBuiltValueObject && foo == other.foo;
  }

  @override
  int get hashCode {
    return $jf($jc(0, foo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SimpleBuiltValueObject')
          ..add('foo', foo))
        .toString();
  }
}

class SimpleBuiltValueObjectBuilder
    implements Builder<SimpleBuiltValueObject, SimpleBuiltValueObjectBuilder> {
  _$SimpleBuiltValueObject _$v;

  String _foo;
  String get foo => _$this._foo;
  set foo(String foo) => _$this._foo = foo;

  SimpleBuiltValueObjectBuilder();

  SimpleBuiltValueObjectBuilder get _$this {
    if (_$v != null) {
      _foo = _$v.foo;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SimpleBuiltValueObject other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SimpleBuiltValueObject;
  }

  @override
  void update(void updates(SimpleBuiltValueObjectBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$SimpleBuiltValueObject build() {
    final _$result = _$v ?? new _$SimpleBuiltValueObject._(foo: foo);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
