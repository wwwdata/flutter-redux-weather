import 'package:flutter/material.dart';
import 'package:flutter_redux_weather/ui/elements/weather_location_tile.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:image_test_utils/image_test_utils.dart';

import '../../dummy_data.dart';

void main() {
  testWidgets('Weather location tile', (WidgetTester tester) async {
    int callCount = 0;
    final VoidCallback mockCallback = () {
      callCount += 1;
    };

    // we need to wrap this because flutter per default replaces all network
    // calls with response code 400 and then the Image.network widget will
    // throw an exception and break the test...
    await provideMockedNetworkImages(() async {
      await tester.pumpWidget(
        MaterialApp(
          home: Material(
            child: WeatherLocationTile(
                weatherLocation: dummyWeatherLocation, onPress: mockCallback),
          ),
        ),
      );

      expect(find.text('Test, DE'), findsOneWidget);
      await tester.tap(find.byType(InkWell));
      expect(callCount, 1);
    });
  });
}
