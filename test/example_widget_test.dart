import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Filthy widget usage :(', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: ExampleWidget(isCool: true, foo: 'yeah'),
      ),
    );
  });
}

class ExampleWidget extends StatelessWidget {
  const ExampleWidget({
    @required this.isCool,
    @required this.foo,
    Key key,
  }) : super(key: key);
  final bool isCool;
  final String foo;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: isCool ? Colors.red : Colors.purple,
      child: Text(foo.toUpperCase()),
    );
  }
}
