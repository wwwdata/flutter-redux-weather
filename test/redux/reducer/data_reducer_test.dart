import 'package:built_collection/built_collection.dart';
import 'package:flutter_redux_weather/models/weather_location.dart';
import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/state/data_state.dart';
import 'package:flutter_redux_weather/redux/reducer/data_reducer.dart';
import 'package:test/test.dart';

import '../../dummy_data.dart';

void main() {
  final DataState dataState = DataState.initial();

  test(
    'ReceiveWeatherLocationData',
    () async {
      final WeatherLocation secondDummy = dummyWeatherLocation
          .rebuild((WeatherLocationBuilder b) => b.id = 666);

      final DataState nextState = dataReducer(
          dataState,
          ReceiveWeatherLocationDataAction(<WeatherLocation>[
            dummyWeatherLocation,
            secondDummy,
          ]));

      expect(
        nextState,
        equals(
          DataState.initial().rebuild(
            (DataStateBuilder b) => b
              ..weatherDataByCityId = MapBuilder<int, WeatherLocation>(
                <int, WeatherLocation>{
                  dummyWeatherLocation.id: dummyWeatherLocation,
                  secondDummy.id: secondDummy,
                },
              ),
          ),
        ),
      );
    },
  );
}
