import 'package:flutter_redux_weather/redux/actions.dart';
import 'package:flutter_redux_weather/redux/epics.dart';
import 'package:flutter_redux_weather/redux/state/app_state.dart';
import 'package:flutter_redux_weather/services/database.dart';
import 'package:flutter_redux_weather/services/weather_api.dart';
import 'package:mockito/mockito.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:test/test.dart';

class MockedWeatherApi extends Mock implements WeatherApi {}

class MockedDatabaseProvider extends Mock implements DatabaseProvider {}

class MockedStore extends Mock implements EpicStore<AppState> {}

void main() {
  final WeatherApi api = MockedWeatherApi();
  final DatabaseProvider database = MockedDatabaseProvider();
  final EpicStore<AppState> store = MockedStore();
  final WeatherEpics epics = WeatherEpics(api, database);

  setUp(() async {
    reset(api);
    reset(database);
    reset(store);
  });

  test('restore', () async {
    final Stream<dynamic> actions =
        Stream<dynamic>.fromIterable(<dynamic>[RestoreStateAction()])
            .asBroadcastStream();
    final List<dynamic> emits = await epics(actions, store).toList();
    expect(emits, hasLength(1));
    verify(database.getSavedLocations()).called(1);
    expect(emits[0], TypeMatcher<ReceiveRestoredStateAction>());
  });
}
