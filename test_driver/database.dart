import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_redux_weather/models/saved_location.dart';
import 'package:flutter_redux_weather/services/database.dart';

import 'database/test_page.dart';

void main() {
  enableFlutterDriverExtension();
  runApp(DatabaseTestApp());
}

class DatabaseTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Database tests',
      home: DatabaseTestPage(),
    );
  }
}

class DatabaseTestPage extends TestPage {
  DatabaseTestPage() : super('Database tests') {
    test('save and load locations', () async {
      final DatabaseProvider provider = DatabaseProvider();
      const String dbName = 'testDatabase.db';
      await provider.deleteDb(dbName);
      await provider.open(dbName);

      final SavedLocation loc1 = SavedLocation(
        (SavedLocationBuilder b) => b
          ..id = 1
          ..name = 'Loc1'
          ..country = 'DE',
      );

      final SavedLocation loc2 = SavedLocation(
        (SavedLocationBuilder b) => b
          ..id = 2
          ..name = 'Loc2'
          ..country = 'US',
      );

      await provider.insertLocation(loc1);
      await provider.insertLocation(loc2);
      final List<SavedLocation> locationsFromDb =
          await provider.getSavedLocations();
      await provider.close();

      expect(locationsFromDb, <SavedLocation>[loc1, loc2]);
    });
  }
}
