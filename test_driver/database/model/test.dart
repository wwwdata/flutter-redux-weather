import 'dart:async';

import 'package:flutter_test/flutter_test.dart';

class Test {
  Test(this.name, this.fn, {bool solo, bool skip})
      : solo = solo == true,
        skip = skip == true;
  final bool solo;
  final bool skip;
  String name;
  Func0<FutureOr<Function>> fn;
}
