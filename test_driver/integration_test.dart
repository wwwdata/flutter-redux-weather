import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  test('finds location and can access detail page', () async {
    final FlutterDriver driver = await FlutterDriver.connect();

    await driver.waitFor(find.byType('FloatingActionButton'));
    await driver.tap(find.byType('FloatingActionButton'));
    await driver.waitFor(find.byType('TextField'));
    await driver.tap(find.byType('TextField'));
    await driver.enterText('Munich');
    await driver.waitFor(find.text('Munich, DE'));
    await driver.tap(find.text('Munich, DE'));
    await driver.waitFor(find.text('Muenchen, DE'));
    await driver.tap(find.text('Muenchen, DE'));
    await driver.waitFor(find.text('Forecast:'));

    await driver.close();
  });
}
