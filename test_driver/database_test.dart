import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  test('all tests are green', () async {
    final FlutterDriver driver = await FlutterDriver.connect();
    await driver.waitFor(find.text('all green'));
    driver.close();
  });
}
