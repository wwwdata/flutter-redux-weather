import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_redux_weather/main.dart';
import 'package:flutter_redux_weather/services/database.dart';

Future<Null> main() async {
  enableFlutterDriverExtension();
  // delete database to start fresh  before test
  final DatabaseProvider database = DatabaseProvider();
  await database.deleteDb();

  runApp(AppContainer());
}
